;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defvar *status-window* nil)

(defclass ncurses-window (window)
 ((io-win :accessor io-win-of)))

(defun init-io-layer ()
 (ncurses:init)
 (ncurses::start_color)
 (ncurses::assume_default_colors ncurses:COLOR_RED ncurses:COLOR_WHITE)
 (make-colour-pair 1 'RED 'WHITE)
 )

(defun cleanup-io-layer ()
 (ncurses:cleanup))

(defun ncurses-window (num-lines num-cols line-offset col-offset)
 (let ((win (make-instance 'ncurses-window :num-cols num-cols
                                           :num-lines num-lines
                                           :col-offset col-offset
                                           :line-offset line-offset)))
  (setf (io-win-of win) (ncurses:newwin (num-lines-of win)
                                        (num-cols-of win)
                                        (line-offset-of win)
                                        (col-offset-of win)))
  (ncurses::wbkgd (io-win-of win) (ncurses::COLOR_PAIR 1))
  (ncurses::wclear (io-win-of win))
  win))
  
(defun io-get-screen-size ()
 (values ncurses:LINES ncurses:COLS))

(defun io-make-window (num-lines num-cols line-offset col-offset)
 (ncurses-window num-lines num-cols line-offset col-offset))

(defun io-close-window (window)
 (ncurses::delwin (io-win-of window)))

(defun io-make-status-window (&optional cursor)
 (let ((win (ncurses-window 6 ncurses:COLS (- ncurses:LINES 6) 0)))
  (when cursor
   (put-cursor-in-window cursor win))
  (setf *status-window* win)
  win))

(defun translate-key-to-PES (key-num)
 (cond
  ((= key-num 9) "<TAB>")
  ((= key-num 10) "<CR>")
  ((= key-num 27) "<ESC>")
  ((< 0 key-num 27) (concatenate 'string "<C-" (string (code-char (+ 64 key-num))) ">"))
  ((= key-num 127) "<Backspace>")
  ((= key-num 60) "<<>")
  (t (string (code-char key-num)))))

(defun get-key-from-io-layer ()
 (let ((raw-key (ncurses:getch))
       (ret nil))
  (when (>= raw-key 0)
        (setf ret (translate-key-to-PES raw-key)))
  ;(format t "io returns ~A ~A~%" ret raw-key)
  ret))

(defun render (cursor)
 (let* ((view   (view-of cursor))
        (buffer (buffer-of cursor))
        (window (window-of view))
        (io-win (io-win-of window))
        (cursor-line (line-of cursor))
        (cursor-screen-line nil)
        (col-offset (col-offset-of window))
        (line-offset (line-offset-of window)))
(flet ((render-line (line start-offset end-offset line-number)
        (let ((text (slot-value line 'text)))
         ;(format t "~A ~A ~A~%" (+ line-number line-offset) col-offset text)
         (ncurses:wmove io-win line-number 0)
         (cond 
          ((not (style-manager-of line))
           (enable-style io-win *default-style*)
           (ncurses:waddstr io-win (subseq text start-offset end-offset)))
          (t
           (dolist (chunk (get-style-chunks line start-offset end-offset))
            (destructuring-bind (style start end) chunk
             (enable-style io-win style)
             (ncurses:waddstr io-win (subseq text start end))))))
         (when (and (eq line cursor-line) (not cursor-screen-line))
          (setf cursor-screen-line line-number)))))
 (format t "Rendering window ~A~%" window)
 (ensure-cursor-in-view cursor)
 (ncurses::werase io-win)
 (for-each-screen-line-in cursor #'render-line)
 (unless (eq window *status-window*)
  (ncurses:wmove io-win (1- (num-lines-of window)) 0)
  (enable-style io-win (list 'WHITE 'BLACK '(NORMAL)))
  (ncurses:waddstr io-win (format nil "~A STATUS LINE---------- ~A,~A" (file-name-of buffer) (line-num-of cursor) (col-of cursor))))
 (unless cursor-screen-line 
  ;(error "Didn't find a cursor on screen!")
  (format t "Didn't find a cursor on screen!")
  (setf cursor-screen-line 0)
    )
 (multiple-value-bind (l c) (floor (col-of cursor) (num-cols-of window))
  (ncurses:wmove io-win (+ cursor-screen-line l) c))
 (ncurses:wrefresh io-win))))


(defparameter *pair-num* 1)
(defparameter *pair-to-id* (make-hash-table :test #'equal))

(defun colour-sym->constant (sym)
  (ecase sym
   (BLACK     ncurses:COLOR_BLACK)
   (RED       ncurses:COLOR_RED)
   (GREEN     ncurses:COLOR_GREEN)
   (YELLOW    ncurses:COLOR_YELLOW)
   (BLUE      ncurses:COLOR_BLUE)
   (MAGENTA   ncurses:COLOR_MAGENTA)
   (CYAN      ncurses:COLOR_CYAN)
   (WHITE     ncurses:COLOR_WHITE)))

(defun attribute-sym->constant (sym)
 (ecase sym
   (NORMAL    ncurses:A_NORMAL)
   (STANDOUT  ncurses:A_STANDOUT)
   (UNDERLINE ncurses:A_UNDERLINE)
   (REVERSE   ncurses:A_REVERSE)  
   (BLINK     ncurses:A_BLINK)
   (DIM       ncurses:A_DIM)
   (BOLD      ncurses:A_BOLD)))

(defun make-colour-pair (pair-num fg bg)
 (let* ((fg (colour-sym->constant fg))
        (bg (colour-sym->constant bg))
        (pair (cons fg bg)))
  (ncurses:init_pair pair-num fg bg)
  (setf (gethash pair *pair-to-id*) pair-num)))

(defun get-colour-id (fg bg)
 (gethash (cons (colour-sym->constant fg) (colour-sym->constant bg)) *pair-to-id*))

(defun get-or-make-colour-pair (fg bg)
  (or (get-colour-id fg bg) (make-colour-pair (incf *pair-num*) fg bg)))

(defun enable-style (io-win style)
 ;(format t "Enabling ~A~%" style)
  (destructuring-bind (fg bg attributes) style
   (let ((attrs (reduce #'logior (mapcar #'attribute-sym->constant attributes))))
    (ncurses::wattrset io-win (logior (ncurses::COLOR_PAIR (get-or-make-colour-pair fg bg)) attrs)))))
