;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defvar *active-register* nil)
(defvar *active-register-needs-updating* nil)
(defvar *register-ring* nil)

(defun register-insert-string (string &key (register *active-register*))
 (when (and register (not (eq *mode* 'insert)))
  (insert-string string :cursor register)))

(defun register-insert-line (string &key (register *active-register*))
 (when (and register (not (eq *mode* 'insert)))
  (insert-line :cursor register)
  (register-insert-string string :register register)))

(defun put-register (&optional (register (first *register-ring*)) (dest *active-cursor*))
 (let ((first-line t))
  (for-each-line (line (buffer-of register))
   (unless first-line
    (insert-line :cursor dest)
    (setf first-line nil))
   (insert-string (slot-value line 'text) :cursor dest))))

(defun yank-to-register (region &optional (register *active-register*))
)

(defun yank-line-to-register (&optional (cursor *active-cursor*) (register *active-register*))
 (register-insert-line (get-text-line-at-point cursor) :register register))

(defun trim-register-ring ()
 (loop :for reg :on *register-ring*
       :for i = 0 :then (incf i)
       :when (>= i 9)
       :do (setf (rest reg) nil)
           (return)))

(defun push-register-to-ring (register)
 (format t "Pushing ~%")
 (dump-buffer register)
 (push register *register-ring*)
 (trim-register-ring))

(defun active-register-needs-updating ()
 (setf *active-register-needs-updating* t))

(defun push-active-register-to-ring ()
 (when *active-register-needs-updating*
  (push-register-to-ring *active-register*))
 (setf *active-register-needs-updating* nil))

(defun dump-reg-ring ()
 (format t "Registers are:~%")
 (format t "*active-register* ~A~%" (get-text-line-at-point *active-register*))
 (loop :for r in *register-ring*
       :for i = 0 :then (incf i)
       :do
       (format t "~a = ~A~%" i (get-text-line-at-point r))))
