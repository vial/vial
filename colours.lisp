;;; This software is free under the zlib license, see LICENSE for details


#|
Changing the representation from a raw form, which is a list of styles specified one style per char,
to a run-length encoded style where you have a list of (style start end) is a bit inefficient.
However, the code is pretty simple this way, without horrible logic flow.

The style-manager class lets you have multiple styles active in a class.  For example, a typical
line will have some syntax highlighting.  The line may also have some parts highlighted for 
a search that is currently active.  If the cursor is in the line, braces () could be highlighted.
When you set a style you specify which layer it belongs to.  The layers are merged together to get
to the final colouring.
|#
(in-package :vial)

(defvar *colours* '(BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE))
(defvar *attributes* '(NORMAL STANDOUT UNDERLINE REVERSE BLINK DIM BOLD))
(defvar *default-style* nil)

(defclass style-manager ()
 ((segments :initform nil :accessor segments-of)
  (syntax   :initform nil :accessor syntax-of)
  (braces   :initform nil :accessor braces-of)
  (search   :initform nil :accessor search-of)
  (visual   :initform nil :accessor visual-of)))

(defun make-style (fg bg attributes)
 (assert (or fg bg attributes))
 (list fg bg (if (listp attributes) attributes (list attributes))))

(defun set-default-style (style)
 (setf *default-style* style))

(defun get-raw (line layer filler)
 (flet ((get-layer (line layer)
         (let ((sm (style-manager-of line)))
          (ecase layer
           (default nil)
           (syntax  (syntax-of sm))
           (braces  (braces-of sm))
           (search  (search-of sm))
           (visual  (visual-of sm))))))
  (let ((chunks (get-layer line layer)))
   (if chunks
    (chunks->raw chunks)
    (let (ret)
     (dotimes (x (length (slot-value line 'text)))
      (push filler ret))
     ret)))))

(defun prnt (sm)
 (if sm
  (progn
   (format t "---------------------~%")
   (format t "segments ~A~%" (segments-of sm))
   (format t "syntax ~A~%" (syntax-of sm))
   (format t "visual ~A~%" (visual-of sm))
   (format t "braces ~A~%" (braces-of sm))
   (format t "search ~A~%" (search-of sm)))
  (format t "SM NIL~%")))

(defun set-style-in-line (line layer style start end)
 (unless (style-manager-of line) 
  (setf (style-manager-of line) (make-instance 'style-manager)))
 (let* ((sm (style-manager-of line))
        (raw-syntax  (get-raw line 'syntax nil))
        (raw-braces  (get-raw line 'braces nil))
        (raw-visual  (get-raw line 'visual nil))
        (raw-search  (get-raw line 'search nil))
        (raw-default (get-raw line 'default *default-style*))
        (target-layer (ecase layer
                       (syntax raw-syntax)
                       (braces raw-braces)
                       (search raw-search)
                       (visual raw-visual))))
  (when style
   (loop :for r :on target-layer
         :for pos :from 0
         :when (and (<= start pos) (< pos end))
         :do (setf (first r) style)))
  (setf (segments-of sm) (raw->chunks (mapcar #'(lambda (visual search braces syntax default)
                                                 (or visual search braces syntax default))
                                       raw-visual raw-search raw-braces raw-syntax raw-default)))

 ;(format t "syntax ~A~%" (raw->chunks raw-syntax))
 ;(format t "visual ~A~%" (raw->chunks raw-visual))
 ;(format t "braces ~A~%" (raw->chunks raw-braces))
 ;(format t "search ~A~%" (raw->chunks raw-search))
 ;(format t "default ~A~%" (raw->chunks raw-default))
 ;(format t "segments ~A~%" (segments-of sm))
  (ecase layer
   (syntax (setf (syntax-of sm) (raw->chunks target-layer)))
   (braces (setf (braces-of sm) (raw->chunks target-layer)))
   (search (setf (search-of sm) (raw->chunks target-layer)))
   (visual (setf (visual-of sm) (raw->chunks target-layer))))
  (when (and (eq *default-style* (style-of (first (segments-of sm))))   ; default style
             (length1-p (segments-of sm)))
   (setf (style-manager-of line) nil))))

(defun remove-layer (line layer)
 ;(format t "remove layer ~A ~A (sm is ~A)~%" line layer (style-manager-of line))
 (let ((sm (style-manager-of line)))
  (when sm
   (ecase layer
    (syntax (setf (syntax-of sm) nil))
    (braces (setf (braces-of sm) nil))
    (search (setf (search-of sm) nil))
    (visual (setf (visual-of sm) nil)))
   (set-style-in-line line layer nil 0 0))))

(defun style-of (style-in-line)
 (first style-in-line))

(defun style-start-of (style-in-line)
 (second style-in-line))

(defun get-style-chunks (line start end)
 ;(prnt (style-manager-of line))
 (let ((chunks nil))
  (dolist (c (segments-of (style-manager-of line)))
   (destructuring-bind (style s e) c
    (cond
     ((< e start)                     nil)
     ((< end s)                       nil)
     ((= s end)                       nil)
     ((and (<= s start) (<= end e))   (push (list style start end) chunks))
     ((and (<= s start) (<= e end))   (push (list style start e) chunks))
     ((and (<= start s) (<= e end))   (push (list style s e) chunks))
     ((and (<= start s) (<= end e))   (push (list style s end) chunks))
   )))
  (nreverse chunks)))

(defun raw->chunks (raw)
 (let ((chunks nil)
       (last-style 'never-see-this-style)
       (last-end (length raw)))
  (loop :for r :in raw
        :for start :from 0
        :do (when (not (equal r last-style))
            (push (list r start) chunks)
            (setf last-style r)))
  (nreverse (mapcar #'(lambda (c) 
                       (let ((ret (append c (list last-end))))
                        (setf last-end (second c))
                        ret)) chunks))))

(defun chunks->raw (chunks)
 (let ((raw nil))
  (dolist (c chunks)
   (destructuring-bind (style start end) c
    (dotimes (x (- end start))
     (push style raw))))
  (nreverse raw)))

(defun line-has-style (line layer)
 (let ((sm (style-manager-of line)))
  (when sm
   (ecase layer
    (syntax (syntax-of sm))
    (braces (braces-of sm))
    (search (search-of sm))
    (visual (visual-of sm))))))
