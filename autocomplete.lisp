;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defclass dictionary ()
 ((words            :initform (binary-trees:make-binary-tree :AVL 
                                                             :compfun #'string< 
                                                             :eqfun #'string-equal
                                                             :keyfun #'identity) :accessor words-of)
  (partial-word     :initform "" :accessor partial-word-of)
  (current-list     :initform nil :accessor current-list-of)
  (place-in-list    :initform nil :accessor place-in-list-of)
  ))

(defun current-completion-of (dictionary)
 (first (place-in-list-of dictionary)))

(defun add-word (dictionary word)
 (binary-trees:insert (words-of dictionary) word))

(defun partial-match-p (partial word)
 (let ((partial-len (length partial)))
  (when (>= (length word) partial-len)
   ;(format t "~A ~A ~A~%" partial word (string-equal partial word :end2 partial-length))
   (string-equal partial word :end2 partial-len))))

(defun get-all-completions-for (dictionary &optional (partial (partial-word-of dictionary)))
 "Does a search of the given dictionary for words that are prefixed with PARTIAL,
  returns a list of completions"
 (when (< 0 (length partial))
  (let* ((tree           (words-of dictionary))
         (partial-length (length partial))
         (ret (list partial)))
   (binary-trees:do-tree-range (data (words-of dictionary) :lower partial)
    (when (not (partial-match-p partial data))
     (return-from get-all-completions-for (nreverse ret)))
    (push data ret))
   (nreverse ret))))

(defun get-whole-dictionary (dictionary)
 (let ((ret (list "")))
  (binary-trees:dotree (w (words-of dictionary))
   (push w ret))
  (nreverse ret)))

(defun prepare-dictionary (dictionary partial)
 ;(format t "prepare-dictionary ~A~%" partial)
 (let ((completions (if partial
                     (get-all-completions-for dictionary partial)
                     (get-whole-dictionary dictionary))))
 (setf (current-list-of dictionary) completions
       (place-in-list-of dictionary) (current-list-of dictionary)
       (partial-word-of dictionary) partial)
 ;(format t "All comps are ~A~%" (current-list-of dictionary))
 (first (place-in-list-of dictionary))))

(defun get-next-completion-for (dictionary)
 "Repeatedly calling this function with the same inputs will
 cycle through all known completions"
 (setf (place-in-list-of dictionary) (rest (place-in-list-of dictionary)))
 (when (null (place-in-list-of dictionary))
  (setf (place-in-list-of dictionary) (current-list-of dictionary)))
 (first (place-in-list-of dictionary)))
