;;; This software is free under the zlib license, see LICENSE for details

; add buffer list for multiple buffers
(in-package :vial)

(defmacro for-each-line ((line buffer) &body body)
  `(loop :for ,line :in (slot-value ,buffer 'lines) :do ,@body))

(defparameter *cursor-list* nil)
(defparameter *cursor-cycle* nil)
(defparameter *buffer-sequence* 0)

(defclass buffer ()
  ((lines           :initform (list (make-instance 'line)))
   (undo-manager    :initform (make-instance 'undo-manager) :accessor undo-manager-of)
   (file-name       :initform nil :accessor file-name-of)
   (file-type       :initform nil :accessor file-type-of)
   (number          :initform nil :accessor number-of)
   (search-marks    :initform nil :accessor search-marks-of)
   (current-search-mark :initform nil :accessor current-search-mark-of)
   (buffer-mappings :initform nil :accessor buffer-mappings-of)
   (hooks           :initform (make-hash-table) :accessor hooks-of)
   ))

(defun bufferp (obj)
 (eq 'buffer (type-of obj)))

; UGLY - inconsistant use of *active-cursor*. Need to move to a scheme where *active-cursor* is 
; used implicitly alot more
(defvar *active-cursor* nil)

(defun find-buffer-by-name (name)
  (find-if (lambda (cursor) (string= name (file-name-of (buffer-of cursor)))) *cursor-list*))

(defun load-file (filename)
 (let ((existing-cursor (find-buffer-by-name filename)))
  (if existing-cursor
   existing-cursor
   (let* ((cursor (new-cursor-and-buffer))
          (buffer (buffer-of cursor))
          (*active-cursor* cursor))
    (when (file-exists-p filename)
     (with-dont-run-hooks
      (with-open-file (file filename :direction :input)
       (with-no-undo 
        (do ((line (read-line file nil 'eof)
              (read-line file nil 'eof)))
         ((eql line 'eof))
         (insert-string line)
         (insert-line))
        (delete-line)))))
    (cursor-move-to cursor 1 0)
    (setf (file-name-of buffer) filename
     (file-type-of buffer) 'lisp
     (number-of buffer) (incf *buffer-sequence*))
    (push cursor *cursor-list*)
    (file-type-specific-init buffer)
    cursor))))

(defun next-buffer ()
 (unless *cursor-cycle*
  (setf *cursor-cycle* *cursor-list*))
 (let ((ret (first *cursor-cycle*)))
  (setf *cursor-cycle* (rest *cursor-cycle*))
  ret))

(defun save-buffer (buffer &optional new-filename)
 (let ((filename (or new-filename (file-name-of buffer))))
  (format t "Saving ~A ~A~%" filename (file-name-of buffer))
  (with-open-file (file filename :direction :output :if-exists :supersede)
   (loop :for line :in (slot-value buffer 'lines)
    :do
    (write-line (slot-value line 'text) file)))))

(defun emg-save-buffers ()
 (dolist (cursor *cursor-list*)
  (let ((buffer (buffer-of cursor)))
   (save-buffer buffer (concatenate 'string (file-name-of buffer) ".emg")))))

(defun new-cursor-and-buffer ()
 (let* ((buf (make-instance 'buffer))
        (view (make-instance 'view))
        (cursor (make-instance 'cursor :buffer buf
                                       :view view)))
  (register-hook buf 'line-changed #'search-handle-line-changed)
  (set-top-line-to-cursor cursor)
  cursor))

(defun num-lines (buffer)
  (length (slot-value buffer 'lines)))

(defun buffer-empty-p (cursor)
 (let ((lines (slot-value (buffer-of cursor) 'lines)))
  (and (null (rest lines)) (string= "" (get-text-line-at-point cursor)))))

(defclass line ()
  ((text          :initform "")
   (marks         :initform nil :accessor marks-of)
   (style-manager :initform nil :accessor style-manager-of)))

#|
  The primitive line manipulation functions, yes there are very
  few - that is on purpose.  If we need to, these can be well optimised.
  We can hook the undo stack in here, and all the higher levels get undo
  functionality with little/no effort.
|#
(defun insert-string (string &key (cursor *active-cursor*))
  (let ((lhs   (get-lhs :cursor cursor))
	(rhs   (get-rhs :cursor cursor)))
    (setf (get-text-line-at-point cursor) (concatenate 'string lhs string rhs))
    (run-hook (buffer-of cursor) 'line-changed cursor (concatenate 'string lhs rhs))
    (save-line-change cursor (concatenate 'string lhs rhs))
    (incf (col-of cursor) (length string))))

(defun insert-line (&key (cursor *active-cursor*) (before-this-line nil))
  (let ((new-line (list (make-instance 'line))))
    (cond 
      ((and before-this-line (= 1 (line-num-of cursor)))
					;(format t "top line replace~%")
       (setf (rest new-line) (slot-value (buffer-of cursor) 'lines)
	     (slot-value (buffer-of cursor) 'lines) new-line)
       (cursor-move-to cursor 1 0)
       (save-inserted-line cursor))
      (before-this-line
       (cursor-move-by cursor -1 0 :col-absolute t)
       (save-inserted-line cursor)
       (insert-line :cursor cursor))
      (t
       (let ((append-after (line-list-of cursor))
	     (rest-after   (rest (line-list-of cursor))))
        ;(format t "insert-line ~A ~A~%" append-after rest-after)
	 (setf (rest new-line) rest-after
	       (rest append-after) new-line)
	 (cursor-move-by cursor 1 0 :col-absolute t)
         (save-inserted-line cursor))))))

(defun tell-views-topline-deleted (old-top cursor)
 (when (eq (top-line-of (view-of cursor)) old-top)
  (setf (top-line-of (view-of cursor)) (slot-value (buffer-of cursor) 'lines))))

(defun delete-line (&key (cursor *active-cursor*))
 (save-deleted-line cursor)
 (register-insert-line (get-text-line-at-point cursor))
  (cond 
    ((= 1 (line-num-of cursor))		; deleting top line
     (with-slots (lines) (buffer-of cursor)
      (let ((old-top lines))
       (setf lines (rest lines))
       (unless lines
        (setf lines (list (make-instance 'line))))
      (tell-views-topline-deleted old-top cursor)
      (cursor-move-to cursor 1 0))))
    (t 
     (cursor-move-by cursor -1 0 :col-absolute t)
     (setf (rest (line-list-of cursor)) (rest (rest (line-list-of cursor)))))))

(defun delete-chars (num-chars &key (cursor *active-cursor*) (to-right nil))
  (let ((lhs            (get-lhs :cursor cursor))
	(rhs            (get-rhs :cursor cursor)))
    (cond
      (to-right 
       (let* ((len        (length rhs))
	      (new-start  (if (< num-chars len) num-chars len))
	      (new-rhs    (subseq rhs new-start)))
         (register-insert-string (subseq rhs 0 new-start))
	 (setf (get-text-line-at-point cursor) (concatenate 'string lhs new-rhs))))
      (t
       (let* ((len            (length lhs))
              (at-end-of-line (zerop (length rhs)))
	      (new-len        (if (< num-chars len) (- len num-chars) 0))
              (chars-to-move  (- new-len len))
	      (new-lhs        (subseq lhs 0 new-len)))
         (register-insert-string (subseq lhs new-len))
	 (setf (get-text-line-at-point cursor) (concatenate 'string new-lhs rhs))
         (format t "Moving back by ~A ~%" chars-to-move)
         (cursor-move-by cursor 0 chars-to-move))))
    (run-hook (buffer-of cursor) 'line-changed cursor (concatenate 'string lhs rhs))
    ; if we are at the end of the line and deleting, need to hack the column length
    (when (zerop (length rhs)) (setf (col-of cursor) (length (get-text-line-at-point cursor))))
    (save-line-change cursor (concatenate 'string lhs rhs))))

;; ------ End of primitive functions
(defun insert-new-line-at-cursor (&key (cursor *active-cursor*))
 (let ((rhs (get-rhs :cursor cursor)))
  (delete-chars (length rhs) :cursor cursor :to-right t)
  (insert-line :cursor cursor)
  (insert-string rhs :cursor cursor)
  (cursor-move-to cursor (line-num-of cursor) 0)))

;; ---------------- TESTING ---------------

(defun dump-buffer (&optional (cursor *active-cursor*))
  (let* ((buf    (buffer-of cursor))
	 (cline  (line-of cursor))
	 (col    (col-of cursor)))
   (format t "DUMPING----------------~%")
    (dolist (line (slot-value buf 'lines))
      (if (eq cline line)
	  (let* ((lhs  (get-lhs))
		 (rhs  (get-rhs))
		 (len  (length lhs)))
	    (format t "~A~%" (slot-value line 'text))
	    (dotimes (x len) (format t " "))
	    (format t "| <- Cursor ~A^~A~%" lhs rhs))
	  (format t "~A~%" (slot-value line 'line))))
   (format t "-----------------------~%")
    (format t "Undo stack is~%")
    (dolist (undo (undo-stack-of (undo-manager-of buf)))
      (format t "~A~%" undo))
    (format t "Redo stack is~%")
    (dolist (redo (redo-stack-of (undo-manager-of buf)))
      (format t "~A~%" redo))))

#|
(test-render)
(setf *active-cursor* (load-buffer "test-file.test"))
(setf *active-cursor* (new-cursor-and-buffer))
(defvar *view* nil)
(setf *view* (make-instance 'view :cursor *active-cursor*))

(apply-undo *active-cursor*)
(apply-redo *active-cursor*)
(dump-buffer)
(format t "~A~%" *key-buffer*)

(handle-newline-key)
(delete-line)
(insert-line)
(insert-string "brad")
(delete-chars 1)
(dump-buffer)

 (insert-string "hello")

(insert-string "brad")
(insert-string "test")
(cursor-move-to *active-cursor* 1 1)
(setf *compress-undos* nil)

(loop for sym being the present-symbols in *package* when (fboundp sym) do (eval `(trace ,sym)))
(loop for sym being the present-symbols in *package* when (fboundp sym) do (eval `(untrace ,sym)))
|#
