;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defclass undo-manager ()
 ((undo-stack :initform () :accessor undo-stack-of)
  (redo-stack :initform () :accessor redo-stack-of)
  (recording-blocks :initform nil :accessor recording-blocks-of)
  (nesting    :initform 0  :accessor nesting-of)))

(defmacro with-undo-block (cursor &body body)
 (let ((c (gensym)))
 `(let ((,c ,cursor)
        (*compress-undos* t))
     (setf (recording-blocks-of (undo-manager-of (buffer-of ,c))) 'block-starting)
     ,@body
     (when (eq (recording-blocks-of (undo-manager-of (buffer-of ,c))) 'block-recording)
      (push-change '(block-stop) ,c)
      (setf (recording-blocks-of (undo-manager-of (buffer-of ,c))) nil))
     )))

; these undo vars need to be made per-buffer
(defvar *suppress-undo* nil)
(defvar *compress-undos* nil)

(defun change-type (change) (first change))
(defun change-line (change) (second change))
(defun deleted-text (change) (third change))
(defun orig-text (change) (third change))
(defun new-text (change) (fourth change))

(defun push-change (change cursor)
 (unless *suppress-undo*
  (let* ((stack     (undo-stack-of (undo-manager-of (buffer-of cursor))))
         (last-undo (first stack)))
   (when (eq (recording-blocks-of (undo-manager-of (buffer-of cursor))) 'block-starting)
     (push '(block-start) (undo-stack-of (undo-manager-of (buffer-of cursor))))
     (setf (recording-blocks-of (undo-manager-of (buffer-of cursor))) 'block-recording))
   (cond
    ((and *compress-undos*                       ; compressing
          last-undo                              ; there is a previous entry
          (eq (change-type change)    'line-changed)      
          (eq (change-type last-undo) 'line-changed)      
          (= (change-line change) (change-line last-undo)))
     (setf (fourth last-undo) (fourth change)))
    (t ; otherwise, just push the change
     (push change (undo-stack-of (undo-manager-of (buffer-of cursor)))))))
 ; anytime a new undo comes in, the redo stack is invalidated
 (setf (redo-stack-of (undo-manager-of (buffer-of cursor))) nil)))

(defun save-line-change (cursor old-text)
 (let ((lnum  (line-num-of cursor))
       (text  (get-text-line-at-point cursor)))
  (push-change (list 'line-changed lnum old-text text) cursor)))

(defun save-inserted-line (cursor)
 (let ((lnum  (line-num-of cursor)))
  (push-change (list 'line-inserted lnum) cursor)))

(defun save-deleted-line (cursor)
 (let ((lnum  (line-num-of cursor))
       (text  (get-text-line-at-point cursor)))
  (push-change (list 'line-deleted lnum text) cursor)))

(defun undo-line-deleted (cursor change)
 (assert (eq (change-type change) 'line-deleted))
 (let ((lnum (change-line change))
       (before t)
       (text (deleted-text change)))
  (when (= lnum (num-lines (buffer-of cursor)))
   (decf lnum)
   (setf before nil))
  (cursor-move-to cursor lnum 0)
  (insert-line :cursor cursor :before-this-line before)
  (insert-string text :cursor cursor)))

(defun redo-line-deleted (cursor change)
 (assert (eq (change-type change) 'line-deleted))
 (let ((lnum (change-line change)))
  (cursor-move-to cursor lnum 0)
  (delete-line :cursor cursor)))

(defun undo-or-redo-line-changed (cursor change undo?)
 (assert (eq (change-type change) 'line-changed))
 (let ((lnum (change-line change))
       (text (if undo? (orig-text change) (new-text change))))
  (cursor-move-to cursor lnum 0)
  (delete-chars (length (get-text-line-at-point cursor)) :to-right t)
  (insert-string text :cursor cursor)))

(defun undo-line-changed (cursor change)
 (undo-or-redo-line-changed cursor change t))

(defun redo-line-changed (cursor change)
 (undo-or-redo-line-changed cursor change nil))

(defun undo-line-inserted (cursor change)
 (assert (eq (change-type change) 'line-inserted))
 (let ((lnum (change-line change)))
  (cursor-move-to cursor lnum 0)
  (delete-line :cursor cursor)))

(defun redo-line-inserted (cursor change)
 (assert (eq (change-type change) 'line-inserted))
 (let ((lnum (1- (change-line change))))
  (cond 
   ((zerop lnum) 
    (cursor-move-to cursor 1 0)
    (insert-line :cursor cursor :before-this-line t))
   (t
    (cursor-move-to cursor lnum 0)
    (insert-line :cursor cursor :before-this-line nil)))))

(defvar *undo-nesting* 0)
(defun apply-undo (&optional (cursor *active-cursor*))
 (let* ((last-undo (pop (undo-stack-of (undo-manager-of (buffer-of cursor)))))
        (*suppress-undo* t))
  (cond
   (last-undo
    (push last-undo (redo-stack-of (undo-manager-of (buffer-of cursor))))
    (ecase (change-type last-undo)
     (block-stop (loop for ret = (apply-undo cursor) then (apply-undo cursor)
                            until (eq ret 'block-start)))
     (block-start 'block-start)
     (line-deleted (undo-line-deleted cursor last-undo))
     (line-changed (undo-line-changed cursor last-undo))
     (line-inserted (undo-line-inserted cursor last-undo))))
   (t nil))))

(defun apply-redo (&optional (cursor *active-cursor*))
 (let* ((last-redo (pop (redo-stack-of (undo-manager-of (buffer-of cursor)))))
        (*suppress-undo* t))
  (cond
   (last-redo
    (push last-redo (undo-stack-of (undo-manager-of (buffer-of cursor))))
    (ecase (change-type last-redo)
     (block-start (loop for ret = (apply-redo cursor) then (apply-redo cursor)
                             until (eq ret 'block-stop)))
     (block-stop 'block-stop)
     (line-deleted (redo-line-deleted cursor last-redo))
     (line-changed (redo-line-changed cursor last-redo))
     (line-inserted (redo-line-inserted cursor last-redo))))
   (t nil))))


