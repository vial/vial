;;; This software is free under the zlib license, see LICENSE for details

; Pattern matching version of the normal key commands
(in-package :vial)

(defvar *motion-scanner* nil)
(defvar *op-scanner* nil)
(defvar *op-part-of-op-motion-scanner* nil)
(defvar *op-motion-scanner* nil)
(defvar *op-pending-scanner* nil)
; (cl-ppcre:create-scanner "[0-9]*([jklhbew$^G]|gg)"))

(defvar *repeat-regex* "([1-9][0-9]*)*")

(defparameter *op-patterns* (make-hash-table :test #'equal))
(defparameter *no-repeat*   (make-hash-table))

(defparameter *motion-list* nil)
(defparameter *op-list* nil)
(defparameter *op-motion-list* nil)
(defparameter *op-pending-list* nil)
(defparameter *repeat-pending-scanner* (cl-ppcre:create-scanner (concatenate 'string "^" *repeat-regex* "$")))
(defparameter *invalid-scanner* (cl-ppcre:create-scanner (concatenate 'string "^" *repeat-regex* "\\D$")))

(defun define-pattern (pattern-type pattern-string func &key (hash-string nil))
 (let ((hash-string (or hash-string pattern-string)))
  (setf (gethash hash-string *op-patterns*) func)
  (when (> (number-of-keys hash-string) 1)
   (push (subseq hash-string 0 (bytes-in-front-key hash-string)) *op-pending-list*)))
 (case pattern-type
  ('op        (push pattern-string *op-list*))
  ('motion    (push pattern-string *motion-list*))
  ('op-motion (push pattern-string *op-motion-list*))))

(defun list->regex-grouping (list)
 (with-output-to-string (str)
  (format str "(")
  (loop :for i on list
        :do
        (format str "~A" (first i))
        (when (rest i)
         (format str "|")))
  (format str ")")))

(defun generate-scanners ()
 (let (op-part-of-op-motion)
  (destructuring-bind (motions ops op-motion op-pending)
  (mapcar #'(lambda (list)
             (concatenate 'string *repeat-regex* (list->regex-grouping list)))
            (list *motion-list* *op-list* *op-motion-list* *op-pending-list*))
  (setf op-part-of-op-motion (concatenate 'string "^" op-motion)
        op-motion         (concatenate 'string "^" op-motion motions)
        op-pending        (concatenate 'string "^" op-pending *repeat-regex* "$")
        ops               (concatenate 'string "^" ops)
        motions           (concatenate 'string "^" motions))
  (setf *motion-scanner* (cl-ppcre:create-scanner motions)
        *op-scanner* (cl-ppcre:create-scanner ops)
        *op-motion-scanner* (cl-ppcre:create-scanner op-motion)
        *op-part-of-op-motion-scanner* (cl-ppcre:create-scanner op-part-of-op-motion)
        *op-pending-scanner* (cl-ppcre:create-scanner op-pending))
#|
  (setf *motion-scanner* motions
        *op-scanner* ops
        *op-part-of-op-motion-scanner* op-part-of-op-motion
        *op-motion-scanner* op-motion
        *op-pending-scanner* op-pending)
  |#
  (format t "op-motion ~A~%" op-motion)
  (format t "op-pending ~A~%" op-pending)
  (format t "ops ~A~%" ops)
  (format t "motions ~A~%" motions))))

(defun match-keys (keystring)
 (loop :for pattern :in (list *motion-scanner* *op-scanner* *op-motion-scanner* *repeat-pending-scanner* *op-pending-scanner* *invalid-scanner*)
       :for pat-sym :in (list 'motion          'op          'op-motion          'repeat-pending 'op-pending          'invalid)
       :do (multiple-value-bind (start end) (cl-ppcre:scan pattern keystring)
           (when start
            (return-from match-keys (values pat-sym end)))))
 (values 'invalid-guess 0))

(defun handles-own-repeat-p (func)
 (gethash func *no-repeat*))

(defun handles-own-repeat (func)
 (setf (gethash func *no-repeat*) t))

(defun get-repeat (keystring)
 (if (case (elt keystring 0)
      ((#\0 #\+ #\-) t)
      (otherwise nil))
  (values 1 keystring )
  (multiple-value-bind (int pos) (parse-integer keystring :junk-allowed t)
   (values (if int int 1) (subseq keystring pos)))))

(defun signal-invalid () (format t "Signal invalid~%") (signal 'abort-command))
(defun signal-pending () (format t "Signal pending~%") (signal 'no-more-keys))

(defun do-op (keystring &key (is-motion nil))
 (multiple-value-bind (repeat op-string) (get-repeat keystring)
  ; don't save motion commands, or undo/redo/. commands to the "last command" string
  (unless (or is-motion 
              (cl-ppcre:scan (concatenate 'string *repeat-regex* "([:.u])|(<C-R>)") keystring))
           (setf *last-command* keystring))
  (format t "do-op Keystring is ~A Repeat is ~A; opstring ~A~%" keystring repeat op-string)
  (let* ((func    (gethash op-string *op-patterns*)))
   (format t "calling func ~A~%" func)
   (if (handles-own-repeat-p func)
    (funcall func repeat)
    (dotimes (i repeat)
     (funcall func)))
   (when is-motion (run-hook (buffer-of *active-cursor*) 'cursor-moved-by-user *active-cursor*)))))

(defun split-op-motion (op-and-motion)
 (multiple-value-bind (start end) (cl-ppcre:scan *op-part-of-op-motion-scanner* op-and-motion)
  (let ((op     (subseq op-and-motion 0 end))
        (motion (subseq op-and-motion end)))
  (values op motion))))

(defun do-op-and-motion (keystring)
 (multiple-value-bind (repeat op-motion-string) (get-repeat keystring)
  (multiple-value-bind (op-string motion-string) (split-op-motion op-motion-string)
   (setf *last-command* keystring)
   (format t "do-op-and-motion ~A:~A~%" op-string motion-string)
   (let ((func   (gethash op-string *op-patterns*))
         (region nil))
    (assert (and op-string (> (length op-string) 0)))
    (if (handles-own-repeat-p func)
     (funcall func repeat)
     (dotimes (i repeat)
      (funcall func (with-region 
                     (do-op motion-string)))))
   (run-hook (buffer-of *active-cursor*) 'cursor-moved-by-user *active-cursor*)))))

; removing all the keys is probably a bit drastic, though in practise should be fine
; don't write testcases with <C-c> in them!
(defun pattern-abort-command ()
 (setf *key-pos-max* (length *key-buffer*))
 (signal 'abort-command))

(defun pattern-match (keystring)
 (unless keystring (return-from pattern-match))
 ;(format t "Pattern matching ~A~%" keystring)
 (multiple-value-bind (action end) (match-keys keystring)
  (let ((keystring (subseq keystring 0 end)))
   (format t "Action ~A End ~A~%" action end)
   (setf *key-pos-max* end
         *key-pos* end)
   (ecase action
    (op             (do-op keystring))
    (motion         (do-op keystring :is-motion t))
    (op-motion      (do-op-and-motion keystring))
    (repeat-pending (signal 'no-more-keys))
    (op-pending     (signal 'no-more-keys))
    (invalid        (pattern-abort-command))
    (invalid-guess  (pattern-abort-command))))))

(defun do-command ()
 ;(format t "Processing command: ~A~%" *key-buffer*)
 (setf *active-register* (new-cursor-and-buffer))
 (with-undo-block *active-cursor*
  (handler-case
   (progn
    (pattern-match *key-buffer*)
    (remove-command-from-buffer)
    (push-active-register-to-ring)
   )
   (no-more-keys (c) (format t "Got signal ~A~%" c)
    (setf *key-pos-max* 0))
   (abort-command (c) (format t "Got signal ~A~%" c)
    (remove-command-from-buffer)))))

; Delete commands
(defun command-d-motion (region)
 (active-register-needs-updating)
 (delete-region region :whole-lines t))
(define-pattern 'op-motion "d" #'command-d-motion)

(defun delete-single-line-from-active ()
 (let ((move-cursor (= 1 (line-num-of *active-cursor*))))
  (delete-line)
  (active-register-needs-updating)
  (unless move-cursor
   (cursor-move-by *active-cursor* 1 0 :col-absolute t))))
(define-pattern 'op "dd" #'delete-single-line-from-active)

(defun command-x (&rest args)
 (active-register-needs-updating)
 (delete-chars 1 :to-right t))
(define-pattern 'op "x" #'command-x)

(defun delete-previous-char (&rest args)
  (unless (= 0 (col-of *active-cursor*))
    (cursor-move-by *active-cursor* 0 -1)
    (command-x)))
(define-pattern 'op "X" #'delete-previous-char)

;;; Change commands
;;FIXME (?): undo behaces different from vim
(defun change-single-line-from-active ()
  (delete-single-line-from-active)
  (cursor-move-by *active-cursor* -1 0) ;UGLY: repeated code, look for '(define-pattern 'op "O"'
  (insert-line)
  (enter-insert-mode))
(define-pattern 'op "cc" #'change-single-line-from-active)
;;FIXME (?): different cw behavior on vim, It should not eat the word separator (space)
;;           It seems a strange behavior of vim though, because in vim there is no difference
;;           between "cw" and "ce" (it seems at least). 
(defun change-region (region)
  (command-d-motion region)
  (enter-insert-mode))
(define-pattern 'op-motion "c" #'change-region)

;;; Subst commands
(defun subst-char ()
  (command-x)
  (enter-insert-mode))
(define-pattern 'op "s" #'subst-char)

;;Is there actually any difference between "cc" and "S" in vim?
(define-pattern 'op "S" #'change-single-line-from-active) 

; Movement commands
(defun command-j (&rest args) (cursor-move-by *active-cursor* 1 0))
(define-pattern 'motion "j" #'command-j)

(defun command-k (&rest args) (cursor-move-by *active-cursor* -1 0))
(define-pattern 'motion "k" #'command-k)

(defun command-h (&rest args) (cursor-move-by *active-cursor* 0 -1 :span-lines t))
(define-pattern 'motion "h" #'command-h)

(defun command-l (&rest args) (cursor-move-by *active-cursor* 0 1 :span-lines t))
(define-pattern 'motion "l" #'command-l)

(defun command-0 () (cursor-move-by *active-cursor* 0 0 :col-absolute t))
(define-pattern 'motion "^0" #'command-0 :hash-string "0")

(defun command-$ () (cursor-move-by *active-cursor* 0 'end))
(define-pattern 'motion "[$]" #'command-$ :hash-string "$")

(defun command-gg () (cursor-move-to *active-cursor* 1 0))
(define-pattern 'motion "gg" #'command-gg)

(defun goto-previous-line-beginning () (cursor-move-by *active-cursor* -1 0 :col-absolute t))
(define-pattern 'motion "-" #'goto-previous-line-beginning)

(defun goto-next-line-beginning () (cursor-move-by *active-cursor* 1 0 :col-absolute t))
(define-pattern 'motion "[+]" #'goto-next-line-beginning :hash-string "+")
; simple up and down
(define-pattern 'motion "<C-D>" (lambda() (cursor-move-by *active-cursor* 15 0)))
(define-pattern 'motion "<C-U>" (lambda() (cursor-move-by *active-cursor* -15 0)))

; simple by word - REALLY UGLY!
(defun move-backward-by-word ()
 (scan-for-chars *active-cursor* " " :backwards t)
 (scan-for-chars *active-cursor* " " :backwards t :not-these-chars t))

(defun move-forward-by-word ()
 (scan-for-chars *active-cursor* " ")
 (scan-for-chars *active-cursor* " " :not-these-chars t))

(define-pattern 'motion "w" #'move-forward-by-word)
(define-pattern 'motion "b" #'move-backward-by-word)
(define-pattern 'motion "z" (lambda () (scan-for-chars *active-cursor* "()"))) 
(define-pattern 'motion "Z" (lambda () (scan-for-chars *active-cursor* "()" :backwards t))) 


(defun command-cap-G (repeat) 
 (let ((num-lines (num-lines (buffer-of *active-cursor*))))
  (if (= 1 repeat)
   (cursor-move-to *active-cursor* num-lines 0)
   (cursor-move-to *active-cursor* (min repeat num-lines) 0))))
(define-pattern 'motion "G" #'command-cap-G)
(handles-own-repeat #'command-cap-G)

; insert commands
(define-pattern 'op "i" #'enter-insert-mode)
(define-pattern 'op "I" (lambda () (cursor-move-by *active-cursor* 0 0 :col-absolute t) (enter-insert-mode)))
(define-pattern 'op "a" (lambda () (cursor-move-by *active-cursor* 0 1) (enter-insert-mode)))
(define-pattern 'op "A" (lambda () (cursor-move-by *active-cursor* 0 'end) (enter-insert-mode)))
(define-pattern 'op "O" (lambda () (cursor-move-by *active-cursor* -1 0) (insert-line) (enter-insert-mode)))
(define-pattern 'op "o" (lambda () (insert-line) (enter-insert-mode)))
; search
(define-pattern 'op "/" #'enter-search-mode)
(define-pattern 'op "n" #'redo-search-forward)
(define-pattern 'op "N" #'redo-search-backward)

; special commands
(define-pattern 'op ":" #'enter-ex-mode)

(defun redo-last-command ()
 "Pushes the last command to the head of the key buffer"
 (remove-command-from-buffer)
 (setf *key-buffer* (concatenate 'string *last-command* *key-buffer*)
       *redoing-command* t))
(define-pattern 'op "[.]" #'redo-last-command :hash-string ".")

; put
(define-pattern 'op "p" #'put-register)

; yank
(defun command-yy (repeat)
 (active-register-needs-updating)
 (dotimes (x repeat)
  (yank-line-to-register)
  (cursor-move-by *active-cursor* 1 0))
 (cursor-move-by *active-cursor* (- repeat) 0))
(define-pattern 'op "yy" #'command-yy)
(handles-own-repeat #'command-yy)

;; join lines

;;FIXME: undo breacks when applied to JOIN done on the last line
(defun join-lines-command ()
  "joins current line with next line, putting a whitespace in between"
  (let ((current-linenumber (line-num-of *active-cursor*)))
    (cursor-move-by *active-cursor* 1 0)
    (if (/= current-linenumber (line-num-of *active-cursor*))      ;if we're not on the last line
	(let ((next-line-text (get-text-line-at-point *active-cursor*)));read next line's text, 
	  (delete-line)                                            ;delete next line
	  (cursor-move-by *active-cursor* 0 'end)                  ;go to end of line and paste
	  (let ((eol-column (col-of *active-cursor*)))             ;the text we read
	    (insert-string " ")            ;UGLY: I'm UGLY! (code repetition)
	    (insert-string next-line-text) ;
	    ;;putting the cursor in between the two lines we've joined
	    (cursor-move-by *active-cursor* 0 eol-column :col-absolute t))))))
(define-pattern 'op "J" #'join-lines-command) 

; undo
(define-pattern 'op "u" #'apply-undo)

(define-pattern 'op "<C-R>" #'apply-redo)

; window commands
(define-pattern 'op "<C-W><C-W>" #'change-to-next-window)
; UGLY
(define-pattern 'op "<C-W>c" (lambda () (setf *active-cursor* (first (close-window *active-cursor*)))))

#|

(defmacro with-region (&body body)
 (let ((region (gensym)))
  `(let ((,region (start-region)))
      ,@body
      (end-region ,region))))


 Valid commands
  {motion}
  op{motion} op is singlechar ^[A-z]?
  {motion} = [0-9]*([bewjklh0$^G]|gg)
|#
