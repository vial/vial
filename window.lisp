;;; This software is free under the zlib license, see LICENSE for details

; UGLY - this whole module ought to be handled better, pretty rough right now
(in-package :vial)

(defparameter *status-window* nil)
(defparameter *all-windows* nil)
(defparameter *window-cycle* nil)
(defparameter *active-window* nil)
; TODO *active-window* isn't used properly, do we even need it??

(defclass window ()
  ((num-cols         :initarg :num-cols    :accessor num-cols-of)
   (num-lines        :initarg :num-lines   :accessor num-lines-of)
   (col-offset       :initarg :col-offset  :accessor col-offset-of)
   (line-offset      :initarg :line-offset :accessor line-offset-of)
   (cursor           :initform nil         :accessor cursor-of)
   ))

#|
 - There are at least 2 windows, the status window and the current buffer
 - The EX command is just a buffer and a window
|#

(defmethod print-object ((win window) stream)
 (format stream "Window: ~A ~A ~A ~A" (num-lines-of win) (num-cols-of win) (line-offset-of win) (cursor-of win)))

(defun init-windows ()
 (setf *all-windows* nil
       *active-window* nil
       *window-cycle* nil)
 (multiple-value-bind (num-lines num-cols) (io-get-screen-size)
  (setf *status-window* (io-make-window 5 num-cols (- num-lines 5) 0))))

(defun open-window (num-lines num-cols line-offset col-offset)
 (let ((io-win (io-make-window num-lines num-cols line-offset col-offset)))
  (push io-win *all-windows*)
  (setf *window-cycle* (rest *all-windows*))
  (format t "Just opened window ~A ~A~%" io-win *all-windows*)
  io-win))

(defun find-window-of-cursor (cursor)
 (format t "Trying to find ~A in ~A~%" cursor *all-windows*)
 (find-if (lambda (win) (eq cursor (cursor-of win))) *all-windows*))

(defun put-cursor-in-window (cursor window)
  (setf (window-of (view-of cursor)) window
        (cursor-of window)           cursor))

(defun show-cursor-in-win (cursor window)
 (put-cursor-in-window cursor window)
 (render cursor))

(defun close-window (cursor)
 (format t "1 ~A~%" *all-windows*)
 (setf *all-windows* (remove-if (lambda (win) (eq cursor (cursor-of win))) *all-windows*))
 (format t "2 ~A~%" *all-windows*)
 (let ((cursors (mapcar (lambda (win) (cursor-of win)) *all-windows*)))
  (delete-all-windows)
  (open-all-cursors cursors)
  cursors))

(defun delete-all-windows ()
 (dolist (win *all-windows*)
  (io-close-window win))
 (setf *all-windows* nil))

(defun open-all-cursors (cursors)
 (multiple-value-bind (num-lines-actual num-cols) (io-get-screen-size)
  (let* ((num-lines          (- num-lines-actual (num-lines-of *status-window*)))
         (lines-per-win      (truncate (/ num-lines (length cursors))))
         (lines-in-first-win (- num-lines (* lines-per-win (1- (length cursors)))))
         (line-offset        0))
   (format t "total lines ~A ~A~%" num-lines line-offset)
   (dolist (cursor cursors)
    (cond
     ((eq cursor (first cursors)) ; first entry
      (show-cursor-in-win cursor (open-window lines-in-first-win num-cols 0 0))
      (incf line-offset lines-in-first-win))
     (t
      (show-cursor-in-win cursor (open-window lines-per-win num-cols line-offset 0))
      (incf line-offset lines-per-win)))))))

(defun split-window (file-to-load)
 (format t "split-window |~A|~%" file-to-load)
 (when file-to-load
  (let ((cursors (cons (load-file file-to-load) (mapcar (lambda (win) (cursor-of win)) *all-windows*))))
   (format t "Cursors are ~A~%" cursors)
   (delete-all-windows)
   (open-all-cursors cursors))))

(defun change-to-next-window ()
 (unless *window-cycle*
  (setf *window-cycle* *all-windows*))
 (setf *active-window* (first *window-cycle*)
       *window-cycle* (rest *window-cycle*))
 (setf *active-cursor* (cursor-of *active-window*)))
