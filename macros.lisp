;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defmacro define-repeat-tracking-function (name params &body body)
 "Just like DEFUN, except you can use the internal function THIS-FUNCTION-CALLED-LAST-KEY
  to determine if this function was called the last time a key was pressed"
 (let ((recur (gensym)))
  `(let ((,recur 0))
      (defun ,name ,params
       (labels ((this-function-called-last-key () (= (1+ ,recur) *total-key-count*)))
        ,@body
        (setf ,recur *total-key-count*))))))

(defmacro when-let (expr &body body)
 `(let ((it ,expr))
     (when it 
      ,@body)))

(defmacro with-no-undo (&body body)
 `(let ((*suppress-undo* t))
     ,@body))

(defmacro with-dont-run-hooks (&body body)
 `(let ((*suppress-hooks* t))
     ,@body))
