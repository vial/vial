;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

#|
Valid hooks
 'line-changed : cursor old-line-text
 'cursor-moved : cursor
 'cursor-moved-by-user : cursor
 'key-pressed  : key
|#
(defparameter *supress-hooks* nil)

(defun run-hook (buffer hook &rest args)
 (unless *supress-hooks*
  (dolist (hook-to-run (gethash hook (hooks-of buffer)))
   (apply hook-to-run args))))

(defun register-hook (buffer hook func)
  (push func (gethash hook (hooks-of buffer))))

(defun deregister-hook (buffer hook)
 (setf (gethash hook (hooks-of buffer)) (delete hook (gethash hook (hooks-of buffer)))))
