;;; This software is free under the zlib license, see LICENSE for details
;;; This file has been cribbed from Climacs

; add buffer list for multiple buffers
;(in-package :vial)
(use-package :flexichain)

;(defmacro for-each-line ((line buffer) &body body)
  ;`(loop :for ,line :in (slot-value ,buffer 'lines) :do ,@body))

(defparameter *cursor-list* nil)
(defparameter *cursor-cycle* nil)
(defparameter *buffer-sequence* 0)

(defun next-buffer ()
 (unless *cursor-cycle*
  (setf *cursor-cycle* *cursor-list*))
 (let ((ret (first *cursor-cycle*)))
  (setf *cursor-cycle* (rest *cursor-cycle*))
  ret))

(defvar *active-cursor* nil)

(defun find-buffer-by-name (name)
  (find-if (lambda (cursor) (string= name (file-name-of (buffer-of cursor)))) *cursor-list*))

(defclass buffer ()
  ((contents :initform (make-instance 'standard-cursorchain))
   (file-name       :initform nil :accessor file-name-of)
   (file-type       :initform nil :accessor file-type-of)
   (number          :initform nil :accessor number-of)
   (buffer-mappings :initform nil :accessor buffer-mappings-of)
   (hooks           :initform (make-hash-table) :accessor hooks-of)
   ))

(defun buffer-substring (buffer start end)

(defclass cursor (right-sticky-flexicursor))
