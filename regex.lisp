;;; This software is free under the zlib license, see LICENSE for details

; This file provides safe wrappers around CL-PPCRE
(in-package :vial)

(defun regex-scan (&rest args)
 (ignore-errors (apply #'cl-ppcre:scan args)))

(defun regex-all-matches (&rest args)
 (ignore-errors (apply #'cl-ppcre:all-matches args)))

(defun regex-create-scanner (&rest args)
 (handler-case 
  (apply #'cl-ppcre:create-scanner args)
  (cl-ppcre:ppcre-syntax-error (condition)
   (format t "Houston, we've got a problem with the string ~S:~%~
    Looks like something went wrong at position ~A.~%~
    The last message we received was \"~?\"."
    (cl-ppcre:ppcre-syntax-error-string condition)
    (cl-ppcre:ppcre-syntax-error-pos condition)
    (simple-condition-format-control condition)
    (simple-condition-format-arguments condition))
   (values))))

(defun literal-string-to-regex-string (string)
 "Convert a string which may contain special regex characters to a string
  that will match the literal.  Ie, would convert 'let*' to 'let[*]'"
 (with-output-to-string (str)
  (loop :for c :across string
        :do
        (cond
         ((find c ".?+*")  (format str "[~C]" c))
         ((find c "[]|") (format str "\\~A" c))
         (t (write-char c str))))))
