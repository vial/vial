;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defparameter *known-file-types* (make-hash-table))

(defun register-new-file-type (file-type init-function)
 (setf (gethash file-type *known-file-types*) init-function))

(defun file-type-specific-init (buffer)
 (when-let (gethash (file-type-of buffer) *known-file-types*)
  (funcall it buffer)))
