;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

; TODO, need to make buffers have a better way to store user data
(defparameter *lisp-mode-per-buffer-data* (make-hash-table))
(defparameter *hook-ok-to-run* t)

; for multiple modes, probably want a defmethod here
(defun lisp-mode-init ()
 (register-new-file-type 'lisp #'lisp-type-specific-init)
 (setf *lisp-syntax-scanner* (create-keyword-scanner)))

(defun lisp-type-specific-init (buffer)
  (register-hook buffer 'cursor-moved-by-user #'lisp-mode-handle-cursor-move)
  (register-hook buffer 'line-changed #'lisp-mode-handle-cursor-move)
  (register-hook buffer 'line-changed #'basic-highlight-keywords-in-line-hook)
  (for-each-line (line buffer)
   (basic-highlight-keywords-in-line line)))

(defun basic-highlight-keywords-in-line-hook (cursor old-text)
 (declare (ignore old-text))
 (remove-layer (line-of cursor) 'syntax)
 (basic-highlight-keywords-in-line (line-of cursor)))

(defun basic-highlight-keywords-in-line (line)
 (loop :for match :on (regex-all-matches *lisp-syntax-scanner* (slot-value line 'text)) :by #'cddr
       :do (let ((start (first match))
                 (end   (second match)))
           (when (and start end)
            (set-style-in-line line 'syntax (make-style 'YELLOW 'WHITE '(NORMAL)) (1+ start) end)))))

(defun lisp-mode-handle-cursor-move (cursor &rest args)
 (declare (ignore args))
 (when *hook-ok-to-run*
  (let ((*hook-ok-to-run* nil))
  (lisp-mode-highlight-inner-braces cursor))))

(defun highlight-endpoints-of-region (region)
 (let* ((c1    (cursor1-of region))
        (c2    (cursor2-of region))
        (line1 (line-of c1))
        (line2 (line-of c2))
        (col1  (col-of c1))
        (col2  (col-of c2)))
  (set-style-in-line line1 'braces (make-style 'black 'green '(normal)) col1 (1+ col1))
  (set-style-in-line line2 'braces (make-style 'black 'green '(normal)) col2 (1+ col2))))

; TODO - restrict the number of lines that we will search for
(defun lisp-mode-highlight-inner-braces (cursor)
 "This function is called every time the cursor moves, and when text changes.  It highlights the current innermost
  brace set.  This involves looking up the current buffer, removing any old highlighting
  and setting the new highlighting"
 (let* ((*hook-ok-to-run* nil)
        (buffer       (buffer-of cursor))
        (last-matches (gethash buffer *lisp-mode-per-buffer-data*))
        (this-match   (match-pair cursor #\( #\))))
 (when last-matches
  (let ((c1 (cursor1-of last-matches))
        (c2 (cursor2-of last-matches)))
;   (format t "c1 ~A c2 ~A~%" (line-num-of c1) (line-num-of c2))
;   (format t "lm c1 is ~A|~A~%" (get-lhs :cursor c1) (get-rhs :cursor c1))
;   (format t "lm c2 is ~A|~A~%" (get-lhs :cursor c2) (get-rhs :cursor c2))
   (remove-layer (line-of (cursor1-of last-matches)) 'braces)
   (remove-layer (line-of (cursor2-of last-matches)) 'braces)
   (setf (gethash buffer *lisp-mode-per-buffer-data*) nil)))
 (when this-match
   (setf (gethash buffer *lisp-mode-per-buffer-data*) this-match)
   (highlight-endpoints-of-region this-match)
   )))

(defun lisp-mode-highlight-outer-braces (cursor)
 (let* ((*hook-ok-to-run* nil)
        (start  (cursor-move-to (make-cursor-copy cursor) 1 0))
        (outer  (scan-for-chars start "("))
        (region (match-pair outer #\( #\))))
  (loop :do
        (when (or (null outer) (null region))
          (return-from lisp-mode-highlight-outer-braces nil))
        (when (cursor-in-region-p cursor region)
          (highlight-endpoints-of-region region)
          (return-from lisp-mode-highlight-outer-braces outer))
        (setf outer  (scan-for-chars (cursor-move-by outer 0 1 :span-lines t) "(")
              region (match-pair outer #\( #\))))))

(defun eval-inner-brace (cursor)
 (let ((region (match-pair cursor #\( #\))))
  (when region
   (let ((str (region->string region)))
    (format t "Region is ~%~A~%" str)
    (format t "Result = ~A~%" (eval (read-from-string str)))))))

(defun macroexpand-1-inner-brace (cursor)
 (let ((region (match-pair cursor #\( #\))))
  (when region
   (let ((str (region->string region)))
    (format t "Region is ~%~A~%" str)
    (format t "Result = ~A~%" (ignore-errors (macroexpand-1 (read-from-string str))))))))

(define-pattern 'op "r" (lambda () (eval-inner-brace *active-cursor*)))
(define-pattern 'op "e" (lambda () (macroexpand-1-inner-brace *active-cursor*)))
