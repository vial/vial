;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defclass generic-stream (sb-gray:fundamental-stream) ())

(defmethod sb-gray:stream-write-string ((stream cursor) string &optional (start 0) (end nil))
 (insert-string (subseq string start (or end (length string))) :cursor stream)
 string)

(defmethod sb-gray:stream-line-length ((stream cursor)) nil)
(defmethod sb-gray:stream-line-column ((stream cursor)) nil)

(defmethod sb-gray:stream-write-char ((stream cursor) char)
 (if (char= char #\Newline)
  (insert-new-line-at-cursor :cursor stream)
  (insert-string (string char) :cursor stream)))

(defmethod sb-gray:stream-terpri ((stream cursor))
  (insert-new-line-at-cursor :cursor stream))

(defclass buffer-stream (sb-gray:fundamental-stream)
 ((buffer :initarg :buffer :accessor buffer-of)
  (cursor :initform nil    :accessor cursor-of)))

(defmethod sb-gray:stream-write-string ((stream buffer-stream) string &optional (start 0) (end nil))
 (insert-string (subseq string start (or end (length string))) :cursor (cursor-of stream))
 string)

(defmethod sb-gray:stream-write-char ((stream buffer-stream) char)
 (if (char= char #\Newline)
  (insert-new-line-at-cursor :cursor (cursor-of stream))
  (insert-string (string char) :cursor (cursor-of stream))))

(defmethod sb-gray:stream-terpri ((stream buffer-stream))
  (insert-new-line-at-cursor :cursor (cursor-of stream)))


