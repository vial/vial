;;; This software is free under the zlib license, see LICENSE for details

;----------------- EX HANDLING -------------
(in-package :vial)

(defvar *ex-functions* (make-hash-table :test #'equal))
(defvar *pre-ex-active-cursor* nil
 "Bound to *active-cursor* upon entering EX mode.  *active-cursor* can then
  be used for the ex mode buffer.  When EX mode finishes it checks to see if 
  *pre-ex-active-cursor* has changed, if it has then *active-cursor* is set
  to *pre-ex-active-cursor*.  In this way, you can change the buffer that becomes
  active when EX mode exits.")

(defparameter *ex-commands* (make-hash-table :test #'equal))
(defparameter *ex-function-dictionary* (make-instance 'dictionary))

(defun ex-function (entry) (car entry))

(defun ex-completion-function-for (entry) (cdr entry))

(defun def-ex-command (names ex-function &optional completion-function)
 (dolist (entry (if (listp names) names (list names)))
  (setf (gethash entry *ex-commands*) (cons ex-function completion-function))
  (add-word *ex-function-dictionary* entry)))

;;; TODO - ugly having file-dictionary like this
(let ((file-dictionary nil))
 (define-repeat-tracking-function file-autocomplete ()
  (unless (this-function-called-last-key)
   (format t "redoing listing~%")
   (setf file-dictionary (make-instance 'dictionary))
   (dolist (filename (files-in-directory))
    (add-word file-dictionary filename)))
  (let ((text (get-text-line-at-point *active-cursor*)))
   (if (= (position #\Space text) (1- (length text)))
    (do-autocomplete-at-cursor *active-cursor* file-dictionary 'whole-dictionary)
    (do-autocomplete-at-cursor *active-cursor* file-dictionary)))
 ))

(defun ex-quit (&rest args)
 (declare (ignore args))
 (setf *vial-running* nil))
(def-ex-command '("q" "quit") #'ex-quit)

(defun ex-invoke-restart (restart-num)
 (let ((num (parse-integer restart-num)))
  (invoke-restart-send *sliver* (level *sliver*) num)))
(def-ex-command '("r" "restart") #'ex-invoke-restart) 

; todo, force reload, :e!
(defun ex-edit (filename)
 (let ((win (find-window-of-cursor *pre-ex-active-cursor*)))
  (setf *pre-ex-active-cursor* (load-file filename))
  (show-cursor-in-win *pre-ex-active-cursor* win)))
(def-ex-command '("e" "edit") #'ex-edit #'file-autocomplete)

; TODO - save to different filename
(defun ex-write (&rest args)
 (save-buffer (buffer-of *pre-ex-active-cursor*)))
(def-ex-command '("w" "write") #'ex-write #'file-autocomplete)

(def-ex-command '("sp" "split") #'split-window #'file-autocomplete)

(defun ex-next-buffer (&rest args)
 (setf *pre-ex-active-cursor* (next-buffer)))
(def-ex-command '("bn" "bnext") #'ex-next-buffer)

(defun ex-clear-current-search (&rest args)
  "clear current search, removes higlight"
  (declare (ignore args))
  (clear-current-search *pre-ex-active-cursor*))

(def-ex-command '("noh" "nohighlight") #'ex-clear-current-search)
(defparameter *ex-cursor* nil)

(defun enter-ex-mode ()
 (unless *ex-cursor* (setf *ex-cursor* (new-cursor-and-buffer)))
 (format t "Entering ex mode~%")
 (let ((*pre-ex-active-cursor* *active-cursor*))
  (let* ((*mode* 'ex)
         (*active-cursor* *ex-cursor*))
   (show-cursor-in-win *ex-cursor* *status-window*)
   (insert-line)
   (insert-string ":")
   (simple-input-loop #'insert-normal-key #'ex-special-key))
  (unless (eq *pre-ex-active-cursor* *active-cursor*)
   (setf *active-cursor* *pre-ex-active-cursor*))))

(defun abort-ex-mode ()
 (leave-simple-input-mode))

;;; TODO
;;; Directory code ought to be in another file
(defun get-pwd ()
 (format nil "~A" *default-pathname-defaults*))

(defgeneric strip-pwd-from-filename (filename))
(defmethod strip-pwd-from-filename ((filename pathname))
 (strip-pwd-from-filename (format nil "~A" filename)))

(defmethod strip-pwd-from-filename ((filename string))
 (let ((pwd (get-pwd)))
  (if (and (>= (length filename) (length pwd))
           (string= pwd filename :end2 (length pwd)))
   (subseq filename (length pwd))
   filename)))

(defun files-in-directory (&optional (dirname *default-pathname-defaults*))
 (mapcar #'strip-pwd-from-filename (cl-fad:list-directory dirname)))

(defun do-ex-autocomplete ()
 (let* ((line  (get-text-line-at-point *active-cursor*))
        (entry (get-ex-entry-from-line line)))
  (format t "do-ex-autocomplete ~A ~A~%" line entry)
  (cond
   ((and entry (ex-completion-function-for entry) (find #\Space line))
    (funcall (ex-completion-function-for entry)))
   (t   (do-autocomplete-at-cursor *active-cursor* *ex-function-dictionary* (subseq line 1))))))

(defun get-rest-from-line (line)
 (let* ((pos   (position #\Space line))
        (rest  (if pos (subseq line (1+ pos)) "")))
  rest))

(defun get-ex-entry-from-line (line)
 (let* ((pos   (position #\Space line))
        (cmd   (if pos (subseq line 1 pos) (subseq line 1))))
  (if cmd (gethash cmd *ex-commands*) nil)))
   
(defun process-current-ex-command ()
 (let* ((line  (get-text-line-at-point *active-cursor*))
        (entry (get-ex-entry-from-line line))
        (rest  (get-rest-from-line line)))
  (format t "ex command! ~A ~%" entry)
  (cond
   (entry (funcall (ex-function entry) rest))
   (t (format t "Not a ex command! ~A ~%" entry)))
  (leave-simple-input-mode)))

(defun ex-special-key (key)
 (format t "do-ex-key-special ~A~%" key)
 (format t "    ~A~%" (get-key))
 (cond
  ((string-equal key "<TAB>")  (do-ex-autocomplete))
  ((string-equal key "<C-C>") (abort-ex-mode))  ; \Cc
  ((string-equal key "<ESC>") (abort-ex-mode))  ; Escape
  ((string-equal key "<CR>") (process-current-ex-command)) ; \Cj or return
  ((string-equal key "<Backspace>") (delete-chars 1) 
                                    (when (zerop (col-of *active-cursor*)) 
                                     (abort-ex-mode)))
 )
(advance-by-key))
