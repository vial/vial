(in-package :vial)

(defparameter *status-cursor* nil)

(defun status-message (message)
 (unless *status-cursor* (setf *status-cursor* (new-cursor-and-buffer)))
 (let* ((*active-cursor* *status-cursor*))
  (insert-line)
  (insert-string message)
  (show-cursor-in-win *status-cursor* *status-window*)))
