;;; This software is free under the zlib license, see LICENSE for details
;(require 'vial)
;(vial::main-loop)
;(vial:vial)
;;;; VIAL - VI Adopts Lisp
(in-package :vial)

(defun fix-me-later (day month note))

(defparameter *vial-running* t)
(defparameter *redoing-command* nil)

; dvorak mappings for movement (Brad types dvorak)
(defun do-some-remaps ()
 (define-key-remap 'normal "h" "j")
 (define-key-remap 'normal "t" "k")
 (define-key-remap 'normal "j" "h")
 (define-key-remap 'normal "k" "l"))

; TODO first run doesn't init the screen???
(defun vial (&optional filename)
 (set-default-style (make-style 'BLACK 'WHITE '(NORMAL)))
 (generate-scanners)
 (init-io-layer)
 (init-lisp-implementation)
 (init-windows)
 (lisp-mode-init)
 (do-some-remaps)

 (setf *cursor-list* nil)
 (setf *vial-running* t)
 (setf *key-buffer* "")
 (setf *active-register* (new-cursor-and-buffer))
 (let* ((*active-cursor* (if filename (load-file filename) (new-cursor-and-buffer))))
  (open-all-cursors (list *active-cursor*))
  (setf *mode* 'normal)
  (loop while *vial-running*
   :do 
   (restart-case 
    (progn
     ;(format t "~A|~A~%" (get-lhs :cursor *active-cursor*) (get-rhs :cursor *active-cursor*))
     (format t "*last-command* ~A~%" *last-command*)
     (render *active-cursor*)
     (unless *redoing-command* 
     ; (get-key-from-keyboard)
      (wait-for-event))
     (setf *redoing-command* nil)
     (do-command))
    (try-to-keep-vial-running () (setf *key-buffer* nil
                                  *mode* 'normal))))
  ;(dump-buffer *active-cursor*)
  ;(dump-reg-ring)
  (cleanup-lisp-implementation)
  (cleanup-io-layer)))

(defun main-loop ()
 (vial "test-file.test"))
