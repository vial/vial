;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defun enter-insert-mode ()
  (format t "enter-insert-mode~%")
  (with-undo-block *active-cursor*
   (let ((*mode* 'insert))
    (simple-input-loop #'insert-normal-key #'insert-special-key))))

(defun insert-normal-key (key)
 (insert-string key))

(defun leave-insert-mode ()
 (setf *last-command* (subseq *key-buffer* 0 *key-pos-max*))
 (leave-simple-input-mode))

(defun insert-special-key (key)
 (format t "do-insert-key-special |~A|~%" key)
 (format t "getkey    ~A~%" (get-key))
 (cond
  ((string-equal key "<TAB>") (insert-string " "))
  ((string-equal key "<<>") (insert-string "<"))
  ((string-equal key "<C-C>")  (format t "1~%") (leave-insert-mode))   ; \Cc
  ((string-equal key "<ESC>")  (format t "2~%") (leave-insert-mode))   ; Escape
  ((string-equal key "<CR>")  (format t "3~%") (insert-new-line-at-cursor))   ; \Cj or return
  ((string-equal key "<Backspace>") (format t "4~%") (delete-chars 1)) ; backspace
 )
 (advance-by-key))
