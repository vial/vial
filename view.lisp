;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defclass view ()
  ((window   :initform nil :accessor window-of)
   (top-line :initform nil :accessor top-line-of)
   (top-line-num :initform nil :accessor top-line-num-of)
   (lines-wrap  :initform t :accessor lines-wrap-for)))

(defun num-screen-lines (line view)
 (if (lines-wrap-for view)
  (1+ (floor (length (slot-value line 'text)) (num-cols-of (window-of view))))
  1))

(defun cursor-in-view? (cursor)
 (let ((view        (view-of cursor))
       (cursor-line (line-of cursor)))
  (for-each-screen-line-in cursor #'(lambda (line s e n) 
                                               (when (eq cursor-line line) 
                                                (return-from cursor-in-view? t))))
  nil))

(fix-me-later 29 9 "There are still problems here.  In non wrap mode, we need to slide sideways.  In wrap mode, lets say a line wraps over 5 screen lines, but we only have a screen of 3 lines.  Advancing the column of the cursor should change the screen line to be rendered, but it won't")
(defun ensure-cursor-in-view (cursor)
 (let ((view (view-of cursor)))
  (cond
   ((< (line-num-of cursor) (top-line-num-of view))
    (set-top-line-to-cursor cursor))
   ((not (cursor-in-view? cursor))
    (set-bottom-line-to-cursor cursor)))))

(defun set-top-line-to-cursor (cursor)
 (let ((view (view-of cursor)))
  (setf (top-line-of view) (line-list-of cursor)
   (top-line-num-of view) (line-num-of cursor))))

#|
  The basic idea is
  - start with the cursor line and figure out how many screen lines it will take up
  - move a line back in the buffer until you've used all the lines of the screen up
  - when there are no more screen lines, you have found the new top line of the view
|#
(defun set-bottom-line-to-cursor (cursor)
 (let* ((view      (view-of cursor))
        (window    (window-of view))
        (win-lines (num-lines-of window))
        (line-count 0))
; Start by making sure that if nothing else, the cursor will be on the top line
  (set-top-line-to-cursor cursor)
  (loop :for line-num = (line-num-of cursor) :then (decf line-num)
        :do
        (multiple-value-bind (line lines) (get-line line-num :cursor cursor)
         ;(format t "current-top is ~A |~A| num-lines~%" line-num (slot-value line 'text) (num-screen-lines line view))
         (incf line-count (num-screen-lines line view))
         (when (or (>= line-count win-lines) (= 1 line-num)) 
          ; we just encountered the last line that will fit on screen, so bail out
          ; the last line that fit properly was set just below in the previous iteration,
          ; or it was set at the start when we put the cursor as the top line
          ;(format t "Bottom line is ~A~%" (slot-value line 'text))
          (return))
         ; each valid line that fits on the screen is our potential new top line, so set the new top line here
         (setf (top-line-of view) lines
               (top-line-num-of view) line-num)
         ))))

(fix-me-later 29 9 "There is a mess up between windows/views, not logical in places")

(fix-me-later 29 9 "Need to allow side scrolling for non wrapped views")
(defun for-each-screen-line-in (cursor func)
 "Func must take (line start-offset end-offset line-number)
  The offsets work as for subseq.
  This function is relatively complex, and it would be nice to make it 
  a bit simpler.  On the upside, it really is two functions - rendering
  with and without text wrapping"
 (assert func)
 (let* ((view     (view-of cursor))
        (window   (window-of view))
        (num-cols (num-cols-of window))
        (num-lines (1- (num-lines-of window)))
        (last-line-len 1))
  (labels ((wrapping-to-view ()
            ;(format t "Top line is ~A~%" (slot-value ( 'textfirst (top-line-of view))))
            (loop :for line :in (top-line-of view)
                  :for text = (slot-value line 'text) :then (slot-value line 'text)
                  :for lnum = (top-line-num-of view) :then (incf lnum)
                  :for scr-line = 0 :then (incf scr-line last-line-len)
                  :while (< scr-line  num-lines)
                  :do
                  (setf last-line-len (num-screen-lines line view))
                  (loop :for render-line = scr-line then (incf render-line)
                   :for offset = 0 then (incf offset num-cols)
                   :while (and (<= offset (length text)) (< render-line num-lines))
                   :do 
                   (let ((end (min (+ offset num-cols) (length text))))
                    (funcall func line offset end render-line)))))
           (no-wrap-to-view ()
            (loop :for line :in (top-line-of view)
                  :for scr-line = 0 :then (incf scr-line)
                  :while (< scr-line num-lines)
                  :do (let ((text (slot-value line 'text)))
                      (if (> (length text) num-cols)
                       (funcall func line 0 num-cols scr-line)
                       (funcall func line 0 nil scr-line))))))
    (if (lines-wrap-for view)
     (wrapping-to-view)
     (no-wrap-to-view)))))
