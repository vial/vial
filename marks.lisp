;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defclass mark (dnode)
 ((line     :initform nil :initarg :line     :accessor line-of)
  (col      :initform nil :initarg :col      :accessor col-of)))
