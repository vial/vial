;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defmacro with-weakly-preserved-cursor (cursor &body body)
 "Use this form if you want to preserve the cursor for the normal code path, but want to
  change the cursor for other exits, ie using the RETURN-FROM form"
 (let ((mark (gensym))
       (curs (gensym)))
  `(let* ((,curs ,cursor)
          (,mark (make-mark ,curs)))
      ,@body
      (cursor-move-to-mark ,curs ,mark))))

(defmacro with-strongly-preserved-cursor (cursor &body body)
 "Use this form if you always want the cursor preserved.  Like with-weakly-preserved-cursor
  except the body is wrapped in an UNWIND-PROTECT"
 (let ((mark (gensym))
       (curs (gensym)))
  `(let* ((,curs ,cursor)
          (,mark (make-mark ,curs)))
      (unwind-protect (progn ,@body))
      (cursor-move-to-mark ,curs ,mark))))

; The cursor is actually between chars, it is always to the left
; of the col that is says, ie if it is at col 0, then it will
; insert before (to the left of) the 1st char
(defclass cursor (generic-stream)
  ((buffer    :initarg :buffer                  :accessor buffer-of)
   (view      :initarg :view                    :accessor view-of)
   (line      :initarg :line      :initform nil :accessor line-of)
   (line-list :initarg :line-list :initform nil :accessor line-list-of)
   (line-num  :initarg :line-num  :initform nil :accessor line-num-of)
   (col       :initarg :col       :initform 0   :accessor col-of)))

#|
(untrace insert-string)
(format *active-cursor* "hello~%")
(write-char #\a *active-cursor*)
(stream-write-sequence *active-cursor* "hello" 0 5)
|#
(defmethod initialize-instance :after ((c cursor) &key)
(let ((buffer (buffer-of c)))
 (when buffer
   (unless (line-of c)      (setf (line-of c) (first (slot-value buffer 'lines))))
   (unless (line-list-of c) (setf (line-list-of c) (slot-value buffer 'lines)))
   (unless (line-num-of c)  (setf (line-num-of c) 1))
   (unless (col-of c)       (setf (col-of c) 0)))
 c))

(defun make-cursor-copy (cursor)
 "BE VERY CAREFUL WITH THIS FUNCTION.  Cursors should not just be
  copied villy-nilly, because they are stored in a window list and
  EQ is used for comparison.  You should never setf *active-cursor*
  to a copy."
 (make-instance 'cursor 
  :buffer    (buffer-of cursor)
  :view      (view-of cursor)
  :line      (line-of cursor)
  :line-list (line-list-of cursor)
  :line-num  (line-num-of cursor)
  :col       (col-of cursor)))

(defun get-text-line-at-point (cursor)
 (slot-value (line-of cursor) 'text))

(defun cursor-at-eol-p (cursor)
  (= (col-of cursor) (length (get-text-line-at-point cursor))))

(defun (setf get-text-line-at-point) (new-text cursor)
 (setf (slot-value (line-of cursor) 'text) new-text))

; this is REALLY not optimised!
; and it is pretty damn UGLY!
(defun cursor-move-by (cursor line-change col-change &key (span-lines nil) (col-absolute nil))
 (let* ((lnum      (+ (line-num-of cursor) line-change))
        (line      (line-of cursor))
        (num-lines (num-lines (buffer-of cursor))))
  (when (> lnum num-lines) (setf lnum num-lines))
  (when (< lnum 1) (setf lnum 1))
  (when (/= lnum (line-num-of cursor))
   (cursor-move-to cursor lnum (col-of cursor)))
  ; cursor is now on the correct line
  (let ((len (if (line-of cursor) 
              (length (get-text-line-at-point cursor))
              0)))
   ;(format t "span ~A len ~A col ~A col-change ~A~%" span-lines len (col-of cursor) col-change)
   (cond 
    ; spanning forward
    ((and span-lines (<= len (+ (col-of cursor) col-change)))
     (let ((amount-to-move col-change)
           (max-lines (num-lines (buffer-of cursor))))
      (loop :while (< 0 amount-to-move)
            :do
            (cond
             ((<= amount-to-move (- (length (get-text-line-at-point cursor)) (col-of cursor)))
              (incf (col-of cursor) amount-to-move)
              (setf amount-to-move 0))
             ((and (= max-lines (line-num-of cursor)) (>= amount-to-move (- (length (get-text-line-at-point cursor)) (col-of cursor))))
              (setf amount-to-move 0)
              (cursor-move-by cursor 0 'end))
             (t
              (decf amount-to-move (length (get-text-line-at-point cursor)))
              (cursor-move-by cursor 1 0 :col-absolute t))))))
    ; spanning backward
    ((and span-lines (< (+ (col-of cursor) col-change) 0))
     (let ((amount-to-move (abs col-change)))
      (loop :while (< 0 amount-to-move)
            :do
            (cond
             ((< amount-to-move (col-of cursor))
              (decf (col-of cursor) amount-to-move)
              (setf amount-to-move 0))
             ((and (= 1 (line-num-of cursor)) (>= amount-to-move (col-of cursor)))
              (setf amount-to-move 0)
              (cursor-move-to cursor 1 0))
             (t
              (decf amount-to-move (1+ (col-of cursor)))
              (cursor-move-by cursor -1 'end))))))
    ; moving to the end of the line
    ((eq 'end col-change) (setf (col-of cursor) (if (< 0 len) len 0)))
    ; moving within the line
    (t
     (let ((col (if col-absolute col-change (+ (col-of cursor) col-change))))
      (when (>= col len) (setf col (1- len)))
      (when (< col 0) (setf col 0))
      (setf (col-of cursor) col)))))
  (run-hook (buffer-of cursor) 'cursor-moved cursor)
  cursor))

(defun cursor-move-to (cursor lnum col)
 ;(format t "cursor-move-to ~A ~A~%" lnum col)
  (let ((buffer (buffer-of cursor)))
    (when (eq lnum 'last-line)
     (setf lnum (length (slot-value (buffer-of cursor) 'lines))))
    (when (or (< lnum 1) (> lnum (num-lines buffer)))
      (format t "cursor-move-to (shouldn't see this) ~A ~A : ~A~%" lnum col (num-lines buffer))
      (break))
    (multiple-value-bind (line line-rest) (get-line lnum :cursor cursor)
      (setf (line-of cursor) line
            (line-num-of cursor) lnum
            (line-list-of cursor) line-rest)
      (let* ((len (if line (length (slot-value line 'text)) 0))
             (new-col (if (eq 'end col) len col)))
       (when (> new-col len) (setf new-col len))
       (when (< new-col 0) (setf new-col 0))
       (setf (col-of cursor) new-col)))
  cursor))

(defun get-line (lnum &key (cursor *active-cursor*))
 (let ((buffer (buffer-of cursor)))
  (when (> 0 (1- lnum)) (setf lnum 1))
  (let ((line (nthcdr (1- lnum) (slot-value buffer 'lines))))
    (values (first line) line))))

(defun get-line-num (line &key (cursor *active-cursor*))
 (1+ (position line (slot-value (buffer-of cursor) 'lines))))

(defun get-lhs-or-rhs (lhs &key (cursor *active-cursor*))
  (let* ((line (line-of cursor))
	 (text (slot-value line 'text))
	 (max-col (length text))
	 (col  (col-of cursor)))
    (when (> col max-col) (setf col max-col))
    (assert (<= 0 col (length text)))
    (if lhs
	(subseq text 0 col)
	(subseq text col))))

(defun get-lhs (&key (cursor *active-cursor*))
  (get-lhs-or-rhs t :cursor cursor))

(defun get-rhs (&key (cursor *active-cursor*))
  (get-lhs-or-rhs nil :cursor cursor))

(defun scan-for-chars (cursor chars &key (backwards nil) (not-these-chars nil))
 (labels ((chars-match (char) (find char chars))
          (scan-line (cursor start end)
           (when (get-text-line-at-point cursor)
            (when (< end (1- (length (get-text-line-at-point cursor)))) (incf end))
            ;(when backwards (format t "Scan line ~A ~A ~A ~A~%" start end (get-text-line-at-point cursor) (subseq (text-of (line-of cursor)) start end)))
            (let ((pos (position-if (if not-these-chars (complement #'chars-match) #'chars-match)
                        (get-text-line-at-point cursor) :start start :end end :from-end backwards)))
             ;(when backwards (format t "pos is ~A~%" pos))
             (when pos
              (cursor-move-by cursor 0 pos :col-absolute t))))))
  (if backwards
   (do-from-cursor-to-start cursor #'scan-line)
   (do-from-cursor-to-end cursor #'scan-line))))

(defun char-at (cursor)
 (let ((text (get-text-line-at-point cursor)))
  (when (and (< 0 (length text)) (< (col-of cursor) (length text)))
   (aref text (col-of cursor)))))

(defun find-unbalanced-pair (cursor lhs rhs &key (backwards nil))
 (let ((chars       (format nil "~A~A" lhs rhs))
       (inc         (if backwards #\) #\())
       (dir         (if backwards -1 1))
       (scan-result t))
  ;(format t "find-unbalanced-pair backwards ~A~%" backwards)
  ;(prnt cursor)
  (loop :with nesting-level = 1
        :until (zerop nesting-level)
        :do
        (unless (scan-for-chars cursor chars :backwards backwards)
         (return-from find-unbalanced-pair nil))
        ;(format t "char-at ~A|~A ~A ~A~%" (get-lhs :cursor cursor) (get-rhs :cursor cursor) inc nesting-level)
        (let ((c (char-at cursor)))
         (if (char= c inc)
          (incf nesting-level)
          (decf nesting-level))
         (when (and (not (zerop nesting-level)) (or (char= c lhs) (char= c rhs)))
          (cursor-move-by cursor 0 dir :span-lines t))))
  cursor))

(defun prnt (cursor)
 (when cursor
  (format t "!!~A ~A~%" (line-num-of cursor) (col-of cursor))))

; TODO - this is some pretty ugly code
(defun match-pair (cursor lhs rhs)
; (format t "trying to match~%")
 (let* ((c      (char-at cursor))
        (cursor (make-cursor-copy cursor))
        (on-lhs (and c (char= c lhs)))
        (on-rhs (and c (char= c rhs)))
        (start  (cond
                 (on-lhs (cursor-move-by (make-cursor-copy cursor) 0 1 :span-lines t))
                 (on-rhs (cursor-move-by (make-cursor-copy cursor) 0 -1 :span-lines t))
                 (t cursor)))
        (start-copy (make-cursor-copy start))
        (c1     (if on-lhs cursor (find-unbalanced-pair start lhs rhs :backwards t)))
        (c2     (if c1 (if on-rhs cursor (find-unbalanced-pair start-copy lhs rhs :backwards nil)))))
  (when (and c1 c2) 
;   (format t "c1 is ~A|~A~%" (get-lhs :cursor c1) (get-rhs :cursor c1))
;   (format t "c2 is ~A|~A~%" (get-lhs :cursor c2) (get-rhs :cursor c2))
;   (format t "~A ~A ~A" c1 c2 *active-cursor*)
   (make-instance 'region :cursor1 c1 :cursor2 c2))))

(define-repeat-tracking-function do-autocomplete-at-cursor (cursor dictionary &optional override-partial)
 (unless (this-function-called-last-key)
  (let* ((text      (get-lhs :cursor cursor))
         (space-pos (position #\Space text :from-end t))
         (partial   (cond
                     ((eq override-partial 'whole-dictionary) nil)
                     (override-partial override-partial)
                     (space-pos        (subseq text (1+ space-pos)))
                     (t ""))))
   (prepare-dictionary dictionary partial)
   (format t "do-autocomplete-at-cursor partial is |~A|~%" partial)))
 (format t "Current is |~A|~%" (current-completion-of dictionary))
 (delete-chars (length (current-completion-of dictionary)) :cursor cursor)
 (let ((next (get-next-completion-for dictionary)))
  (format t "Next is ~A~%" next)
 (insert-string next :cursor cursor)))

(defun cursor< (c1 c2)
 (assert (eq (buffer-of c1) (buffer-of c2)))
 (let ((lc1 (line-num-of c1))
       (lc2 (line-num-of c2)))
  (or (< lc1 lc2)
   (and (= lc1 lc2) (< (col-of c1) (col-of c2))))))

(defun cursor= (c1 c2)
 (assert (eq (buffer-of c1) (buffer-of c2)))
 (and (= (line-num-of c1) (line-num-of c2)) (= (col-of c1) (col-of c2))))

(defun cursor<= (c1 c2)
 (format t "~A ~A < ~A ~A ? ~A~%" (line-num-of c1) (col-of c1) (line-num-of c2) (col-of c2) (cursor< c1 c2))
 (or (cursor< c1 c2) (cursor= c1 c2)))

(defun cursor> (c1 c2)
 (assert (eq (buffer-of c1) (buffer-of c2)))
 (let ((lc1 (line-num-of c1))
       (lc2 (line-num-of c2)))
  (or (> lc1 lc2)
   (and (= lc1 lc2) (> (col-of c1) (col-of c2))))))

(defun cursor>= (c1 c2)
 (or (cursor> c1 c2) (cursor= c1 c2)))

(defun do-from-start-to-cursor (cursor func)
 "Calls FUNC for each cursor position from the start of the buffer until the cursor.
  Stops if FUNC returns a non NIL value, stop and return that value.
  FUNC must accept 3 arguments, the CURSOR, the START of text and the END of text"
 (let ((last-line (line-num-of cursor))
       (start-col (col-of cursor)))
  (flet ((do-call () 
          (when-let (funcall func cursor 
                             0                                          ; start
                             (if (= (line-num-of cursor) last-line) 
                              start-col                                 ; last line, short length 
                              (length (get-text-line-at-point cursor))))     ; regular line, full length
          (return-from do-from-start-to-cursor it))))
   (loop :for i = (cursor-move-to cursor 1 0) :then (cursor-move-by cursor 1 0)
         :do (do-call)
         :until (= (line-num-of cursor) last-line))))
 (values nil))

(defun do-from-cursor-to-start (cursor func)
 "Calls FUNC for each cursor position from the cursor backwards until the start of the buffer.
  Stops if FUNC returns a non NIL value, stop and return that value.
  FUNC must accept 3 arguments, the CURSOR, the START of text and the END of text"
  (let ((first-line (line-num-of cursor)))
   (flet ((do-call () 
           (when-let (funcall func cursor 0                             ; start always 0
                             (if (= (line-num-of cursor) first-line) 
                              (col-of cursor)                           ; first line, short length 
                              (length (get-text-line-at-point cursor))))     ; regular line, full length
            (return-from do-from-cursor-to-start it))))
   (loop :for i = nil :then (cursor-move-by cursor -1 0 :col-absolute t)
         :do (do-call)
         :until (= (line-num-of cursor) 1))
   (format t "do-from-cursor-to-start returning nil~%")
   (values nil))))

(defun do-from-cursor-to-end (cursor func)
 "Calls FUNC for each cursor position from the cursor until the end of the buffer.
  Stops if FUNC returns a non NIL value, stop and return that value
  FUNC must accept 3 arguments, the CURSOR, the START of text and the END of text"
 (flet ((do-call () 
         (when-let (funcall func cursor (col-of cursor) (length (get-text-line-at-point cursor)))
          (return-from do-from-cursor-to-end it))))
  (loop :for i = nil :then (cursor-move-by cursor 1 0 :col-absolute t)
        :do (do-call)
        :until (null (rest (line-list-of cursor)))))
 (values nil))

(defun do-from-end-to-cursor (cursor func)
 "Calls FUNC for each cursor position from the end of the buffer back to the cursor.
  Stops if FUNC returns a non NIL value, stop and return that value
  FUNC must accept 3 arguments, the CURSOR, the START of text and the END of text"
  (let ((last-line (line-num-of cursor))
        (start-col (col-of cursor)))
   (flet ((do-call () 
           (when-let (funcall func cursor 
                      (if (= last-line (line-num-of cursor))
                       start-col                           ; reached the cursor, stop at this col
                       0)                                  ; full line
                      (length (get-text-line-at-point cursor))) ; end, full line
            (return-from do-from-end-to-cursor it))))
    (loop :for i = (cursor-move-to cursor 'last-line 0) :then (cursor-move-by cursor -1 0)
          :do (do-call)
          :until (= last-line (line-num-of cursor)))
   (values nil))))


(defun do-from-cursor-to-cursor (cursor func &key (backwards nil))
 "Call FUNC for each line in the buffer, starting with the line the cursor is on.
  If FUNC returns non NIL at any stage, then the loop stops.  The first time FUNC is
  called, it is with the input CURSOR, after that it is called with the cursor moved
  to the beginning of the line.  The last call to FUNC will be have cursor set to the
  start of the same line as CURSOR, effectively calling the function twice for the same
  line.
  FUNC must accept 3 arguments, the CURSOR, the START of text and the END of text"
  (let ((first-pass  (if backwards #'do-from-cursor-to-start #'do-from-cursor-to-end))
        (second-pass (if backwards #'do-from-end-to-cursor   #'do-from-start-to-cursor)))
   (with-weakly-preserved-cursor cursor
    (let ((start-mark (make-mark cursor)))
     (when-let (funcall first-pass cursor func)
      (return-from do-from-cursor-to-cursor it))
     (cursor-move-to-mark cursor start-mark)
     (when-let (funcall second-pass cursor func)
      (return-from do-from-cursor-to-cursor it))))))

(defun cursor-move-to-mark (cursor mark)
 (cursor-move-to cursor (get-line-num (line-of mark) :cursor cursor) (col-of mark)))

(defun make-mark (&optional (cursor *active-cursor*))
 (let ((mark (make-instance 'mark)))
  (setf (line-of mark)     (line-of cursor)
        (col-of mark)      (col-of cursor))
  mark))

(defun activate-cursor (cursor)
 "Set CURSOR to be the *ACTIVE-CURSOR* and display it"
)
