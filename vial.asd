;;; This software is free under the zlib license, see LICENSE for details

(defpackage :vial-asd
 (:use :cl :asdf))
(in-package :vial-asd)

(defsystem vial
 :name vial
 :version "0.0"
 :maintainer "Brad Beveridge"
 :author "Brad Beveridge"
 :licence "LGPL"
 :description "VIAL"
 :long-description "VI Adopts Lisp.  A VI implementation that aims to have a feature set comparable to VIM, written entirely in Common Lisp."
 :serial t
 :components 
    (
     (:module "sliver"
              :components ((:file "package")
                           (:file "macros" :depends-on ("package"))
                           (:file "sliver" :depends-on ("macros"))
                           (:file "swank-protocol" :depends-on ("sliver"))
                           (:file "sliver-impl" :depends-on ("swank-protocol"))))
     (:file "package")
#+sbcl(:file "impl-sbcl")
     (:file "dlist")
     (:file "macros")
     (:file "file-types")
     (:file "hooks")
     (:file "marks")
     (:file "buffer")
     (:file "window")
     (:file "registers")
     (:file "key-handler")
     (:file "vial")
     (:module ncurses-cffi
	      :components
	      ((:file "ncurses")
	       (:file "ncurses-cffi")))
     (:file "view")
     (:file "undo")
     (:file "cursor")
; implementation specific streams
#+sbcl(:file "impl-stream-sbcl")
     (:file "autocomplete")
     (:file "regions")
     (:file "key-insert")
     (:file "ex-commands")
     (:file "status-line")
     (:file "regex")
     (:file "search")
     (:file "key-normal")
     (:file "colours")
     (:file "ncurses-io")
     (:file "syntax-lisp")
     (:file "mode-lisp")
     (:file "sliver-vial")
     )
:depends-on ("cffi" "cl-fad" "cl-ppcre" "trees" "usocket"))
