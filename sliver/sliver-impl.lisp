(in-package :sliver)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functions that will need to be provided per-implementation
(defun auto-call-stream (sliver stream)
  #+sbcl(sb-sys:add-fd-handler (sb-sys:fd-stream-fd stream) :input 
           (lambda  (_) (poll-for-events sliver)))
)

(defun send-sigint (sliver)
 (format t "Sending sigint~%")
 (let ((pid (slot-value (slot-value sliver 'swank-info) 'swank-pid)))
 #+sbcl(sb-posix:kill pid 2)
 ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun use-sigint-for-interrupt (sliver)
 (ecase (slot-value (slot-value sliver 'swank-info) 'communication-style)
  ((:fd-handler nil) t)
  ((:spawn :sigio)   nil)))

(defun prin1-to-string-for-emacs (object)
  (with-standard-io-syntax
    (let ((*print-case*     :downcase)
          (*print-readably* nil)
          (*print-pretty*   nil))
      (prin1-to-string object))))

(defmethod dispatch-event (sliver event)
 (:documentation "The main event processing function for incoming and outgoing events.  Not part of the external protocol.  All events that start within Sliver begin with :emacs-, all other events come back from Swank."))

(defmethod dispatch-event ((sliver sliver) event)
 (with-slots (continuations continuation-counter swank-stream) sliver
  ;(debug-message sliver "dispatch-event ~a~%" event)
  (when event
   (destructure-case event
    ((:open-dedicated-output-stream port)
     (open-dedicated-stream sliver port))

    ((:emacs-rex form package thread continuation)
     (let ((id (incf continuation-counter)))
      (push (cons id continuation) continuations)
      (swank-protocol:encode-message `(:emacs-rex ,form ,package ,thread ,id) swank-stream)))

    ((:return value id)
     (let ((rec (assoc id continuations)))
      (cond 
       (rec (setf continuations
             (remove rec continuations))
        (funcall (cdr rec) value))
       (t (format t "!!Unexpected return~%")))))

    ((:indentation-update alist) 
     (handle-indentation-update sliver alist))

    ((:write-string output)
     (swank-output sliver output))

    ((:debug thread level condition restarts frames conts)
     (declare (ignore conts))
     (assert thread)
     (init-debugger-level sliver thread level condition restarts frames))

    ((:debug-activate thread level)
     (assert thread)
     (activate-debugger-level sliver thread level))

    ((:debug-return thread level stepping)
     (exit-debugger sliver thread level stepping))

    ((:new-features features)
     (format t "indent update ~A~%" (prin1-to-string-for-emacs event)))

    ((:emacs-interrupt thread)
     (cond 
      ((use-sigint-for-interrupt sliver) (send-sigint sliver))
      (t (swank-protocol:encode-message `(:emacs-interrupt ,thread)))))

    ;;; Below here is not really supported just yet
    ;((:new-package package prompt-string)
            ;(interface:new-listener-package package prompt-string))
    ))))

(defgeneric eval-async (sliver sexp cont package)
 (:documentation "Evaluate SEXP on the Lisp backend that is associated with the Sliver instance.  This function isn't part of the external protocol."))

(defmethod eval-async ((sliver sliver) sexp cont package)
 (rex sliver (cont) (sexp package)
  ((:ok result)
   (when cont (funcall cont result)))
  ((:abort &optional reason) (debug-message sliver (or reason "Evaluation aborted")))))

(defmethod request-connection-info (sliver &optional callback)
 (:documentation "Issue a request to Swank asking for info about the backend.  Not part of the external protocol."))

(defmethod request-connection-info ((sliver sliver) &optional callback)
 (let ((callback (if callback callback
                  (lambda (result) (set-connection-info sliver result)))))
  (eval-async sliver '(swank:connection-info) callback *default-package*)))

(defgeneric set-connection-info (sliver info)
 (:documentation "The default callback for REQUEST-CONNECTION-INFO.  It defaultly fills out the SWANK-INFO slot of the sliver instance.  Not part of the external protocol."))

(defmethod set-connection-info ((sliver sliver) info)
 (let ((swank-info (make-instance 'backend-swank-info)))
  (destructuring-bind (&key pid style lisp-implementation 
                            machine features package wire-protocol-version) info
   (setf (slot-value swank-info 'protocol-version) wire-protocol-version)
   (with-slots (swank-pid communication-style lisp-features) swank-info
    (setf swank-pid pid
          communication-style style
          lisp-features features))
   (destructuring-bind (&key name prompt) package
    (with-slots (lisp-package lisp-package-prompt) swank-info
     (setf lisp-package name
           lisp-package-prompt prompt)))
   (destructuring-bind (&key type name version) lisp-implementation
    (with-slots (lisp-type lisp-version lisp-name host) swank-info
     (setf lisp-type type
           lisp-version version
           lisp-name name)))
   ;          host name;(slime-generate-connection-name name)
   (destructuring-bind (&key instance type version) machine
    (with-slots (machine-instance machine-type) swank-info
     (setf machine-instance instance
           machine-type     type))))
  (setf (slot-value sliver 'swank-info) swank-info)))

(defgeneric open-dedicated-stream (sliver port)
 (:documentation "Swank may request that we use a dedicated port for streams output, this function is called if that request is made.  Any messages coming in on the port that is opened will be directed to SWANK-OUTPUT"))

; TODO - error handling for sockets
(defmethod open-dedicated-stream ((sliver sliver) port)
 (debug-message sliver "Opening dedicated port:~A~%" port)
 (with-slots (io-buffer swank-io-socket swank-io-stream host polling-mode-p) sliver
  (setf io-buffer       (make-array 1024 :element-type 'base-char :fill-pointer 0 :adjustable t)
        swank-io-socket (usocket:socket-connect host port)
        swank-io-stream (usocket:socket-stream swank-io-socket))
  (unless polling-mode-p (auto-call-stream sliver swank-io-stream))))

;;; external protocol functions below here

(defmethod connected-p ((sliver sliver))
 (slot-value sliver 'swank-info))

; TODO - error handling for sockets
(defmethod connect ((sliver sliver) &optional (host "localhost") (port 4005))
 (with-slots (swank-socket swank-stream connected-p) sliver
  (setf swank-socket (usocket:socket-connect host port)
        swank-stream (usocket:socket-stream swank-socket)
        connected-p t))
 (message sliver "Sliver connected to Swank on ~A at port ~A~%" host port)
 (with-slots (polling-mode-p swank-stream) sliver
  (unless polling-mode-p (auto-call-stream sliver swank-stream)))
 (setf (slot-value sliver 'host) host)
 (request-connection-info sliver))

(defmethod disconnect ((sliver sliver) &optional terminate)
 (with-slots (swank-socket swank-stream connected-p) sliver
  (usocket:socket-close swank-socket)
  (setf swank-stream nil
        connected-p nil))
 (message sliver "Sliver disconnected from Swank~%"))

(defmethod poll-for-events ((sliver sliver))
 ;; handle the main communication socket
 (with-slots (swank-stream protocol-buffer) sliver
  (loop :while (listen swank-stream)
        :do (vector-push-extend (read-char swank-stream) protocol-buffer))
  (loop :for message = (swank-protocol:decode-message protocol-buffer)
        :while message
        :do (dispatch-event sliver message)))
 ;; handle the dedicated socket, if it is open
 (with-slots (swank-io-stream io-buffer) sliver
  (when (and swank-io-stream (listen swank-io-stream))
   (loop :while (listen swank-io-stream)
         :do (vector-push-extend (read-char swank-io-stream) io-buffer))
   (swank-output sliver io-buffer)
   (setf (fill-pointer io-buffer) 0))))

(defmethod handle-indentation-update ((sliver sliver) indent-info)
 (declare (ignore indent-info))
 nil)

(defmethod interrupt ((sliver sliver))
 (let ((thread (slot-value sliver 'current-thread)))
  (dispatch-event sliver `(:emacs-interrupt ,thread))))
