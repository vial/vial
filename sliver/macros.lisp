(in-package :sliver)

(defvar *current-thread* nil)

(defmacro define-exchange (exchange-name send-lambda protocol-form &key documentation (response-lambda '(result)) custom-callback)
"A macro for defining Swank exchanges.
Exchanges involve Sliver making a request of Swank, and Swank responding to that request some time later in an asynchronous manner.  The Sliver protocol is to have two functions for each Swank message, one function to start the message and a function that will be called back when Swank returns results.
This macro will generate two generic functions exchange-name-SEND and exchange-name-RESPONSE, most commonly clients of Sliver will override exchange-name-RESPONSE.  The default RESPONSE function simply returns NIL.
<exchange-name>-SEND has the signature (sliver send-lambda).
<exchange-name>-RESPONSE usually has the signature (sliver result), though a different lambda list for RESPONSE can be provided by using the key :RESPONSE-LAMBDA.  If you use :RESPONSE-LAMBDA, the first argument is automatically SLIVER, you only need to provide the rest of the arguments.  If you define a custom :RESPONSE-LAMBDA you must also define :CUSTOM-CALLBACK."
 (let ((send     (intern (format nil "~A-SEND" exchange-name)))
       (response (intern (format nil "~A-RESPONSE" exchange-name)))
       (doc      (format nil "Macro generated SEND/RESPONSE pair. ~A" documentation))
       (callback (gensym)))
  `(progn
      (defgeneric ,send ,(cons 'sliver send-lambda) (:documentation ,doc))
      (defmethod ,send ,(append '((sliver sliver)) send-lambda)
       (let ((,callback ,(if custom-callback
                           custom-callback
                          `(lambda (result) (,response sliver result)))))
        (eval-async sliver ,protocol-form ,callback *default-package*)))
      (defgeneric ,response ,(cons 'sliver response-lambda) (:documentation ,doc))
      (defmethod ,response ,(append '((sliver sliver)) response-lambda) nil))))

(defmacro define-inspector-exchange (exchange-name send-lambda protocol-form &key documentation)
 "Many inspector functions simply call INSPECT-RESPONSE as their response.  For those functions, you do not need a lot of useless *-RESPONSE functions."
 (let ((send     (intern (format nil "~A-SEND" exchange-name)))
       (doc      (format nil "Macro generated SEND for the inspector.  INSPECT-RESPONSE will be the response function. ~A" documentation))
       (callback (gensym)))
  `(progn
      (defgeneric ,send ,(cons 'sliver send-lambda) (:documentation ,doc))
      (defmethod ,send ,(append '((sliver sliver)) send-lambda)
       (let ((,callback (lambda (result) (when result 
                                          (destructuring-bind (&key title type content id) result
                                           (inspect-response sliver title type content))))))
        (eval-async sliver ,protocol-form ,callback *default-package*))))))



(defmacro destructure-case (value &rest patterns)
  "Dispatch VALUE to one of PATTERNS.
A cross between `case' and `destructuring-bind'.
The pattern syntax is:
  ((HEAD . ARGS) . BODY)
The list of patterns is searched for a HEAD `eq' to the car of
VALUE. If one is found, the BODY is executed with ARGS bound to the
corresponding values in the CDR of VALUE."
  (let ((operator (gensym "op-"))
	(operands (gensym "rand-"))
	(tmp (gensym "tmp-")))
    `(let* ((,tmp ,value)
	    (,operator (car ,tmp))
	    (,operands (cdr ,tmp)))
       (case ,operator
         ,@(loop for (pattern . body) in patterns collect 
                   (if (eq pattern t)
                       `(t ,@body)
                       (destructuring-bind (op &rest rands) pattern
                         `(,op (destructuring-bind ,rands ,operands 
                                 ,@body)))))
         ,@(if (eq (caar (last patterns)) t)
               '()
               `((t (format t "No case!! ~A~%" ,value) (error "destructure-case failed: ~S" ,tmp))))))))

; todo - package support is incorrect for the swank end, is it still??
(defmacro rex (sliver (&rest saved-vars)
                      (sexp &optional 
                            (package '(current-package))
                            (thread '*current-thread*))
                      &rest continuations)
   "(rex (VAR ...) (SEXP &optional PACKAGE THREAD) CLAUSES ...)
     
   Remote EXecute SEXP.

   VARs are a list of saved variables visible in the other forms.  Each
   VAR is either a symbol or a list (VAR INIT-VALUE).
            
   SEXP is evaluated and the princed version is sent to Lisp.

   PACKAGE is evaluated and Lisp binds *BUFFER-PACKAGE* to this package.
   The default value is (current-package).

   CLAUSES is a list of patterns with same syntax as `destructure-case'.
   The result of the evaluation is dispatched on CLAUSES.  The result is
   either a sexp of the form (:ok VALUE) or (:abort).  CLAUSES is
   executed asynchronously."
  (let ((result (gensym)))
    `(let ,(loop for var in saved-vars
                         collect (etypecase var
                                   (symbol (list var var))
                                   (cons var)))
       (dispatch-event sliver
        (list :emacs-rex ,sexp ,package ,thread
              (lambda (,result)
                (destructure-case ,result
                  ,@continuations)))))))


(defmacro with-sliver-callback ((function sliver &rest args) callback-args &body body)
 (let ((callback (gensym)))
  `(let ((,callback (lambda ,callback-args ,@body)))
      (,function ,sliver ,callback ,@args))))
