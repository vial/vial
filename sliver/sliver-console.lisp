(in-package :sliver)

;;; example implementation here

(defclass debug-level ()
 ((thread    :initform nil :initarg :thread    :accessor thread-of)
  (level     :initform nil :initarg :level     :accessor level-of)
  (condition :initform nil :initarg :condition :accessor condition-of)
  (restarts  :initform nil :initarg :restarts  :accessor restarts-of)
  (backtrace :initform nil :initarg :backtrace :accessor backtrace-of)
 ))

(defclass sliver-debug-mixin ()
 ((debug-levels :initform (make-hash-table) :accessor debug-levels)
  (active-level :initform nil               :accessor active-level)))

(defclass sliver-console (sliver sliver-debug-mixin) ())

(defmethod init-debugger-level ((mixin sliver-debug-mixin) thread level condition restarts frames)
 (let ((new-level (make-instance 'debug-level
                   :thread thread
                   :level level
                   :condition condition
                   :restarts restarts
                   :backtrace frames)))
  (assert (null (gethash level (debug-levels mixin))))
  (setf (gethash level (debug-levels mixin)) new-level
        (active-level mixin) new-level)))

(defmethod activate-debugger-level ((mixin sliver-debug-mixin) thread level))

(defmethod message ((sliver sliver-console) control-string &rest args)
 (apply #'format (append (list t control-string) args)))

(defmethod swank-output ((sliver sliver-console) string)
 (write-string "!" *standard-output*)
 (write-string string *standard-output*))

(defmethod debug-message ((sliver sliver-console) control-string &rest args)
 (apply #'format (nconc (list t control-string) args)))

(defmethod compile-file-response ((sliver sliver-console) result)
 (format t "compile-file-response got back ~A~%" result))

(defmethod compile-notes-response ((sliver sliver-console) result)
 (format t "get-compile-notes got back ~A~%" result))

(defmethod eval-response ((sliver sliver-console) result)
 (format t "eval-send returned ~A~%" result))

(defmethod repl-eval-response ((sliver sliver-console) result)
 (format t "repl-eval-send returned ~A~%" result))

(defmethod init-debugger-level :after ((sliver sliver-console) thread level condition restarts frames)
 (print-debug-level (active-level sliver)))

(defmethod activate-debugger-level ((sliver sliver-console) thread level)
 (format t "Activating debugger level~%")
 (format t "Thread ~A~%" thread)
 (format t "Level ~A~%" level))

(defmethod exit-debugger ((sliver sliver-console) thread level stepping)
 (with-slots (debug-levels active-level) sliver
  (format t "Exiting debugger level ~A~%" level)
  (setf (gethash level debug-levels) nil)
  (when (and active-level (= (level-of active-level) level))
   (setf active-level nil))))

(defmethod inspect-response ((sliver sliver-console) title type content)
 (flet ((print-ispec (ispec)
          (if (stringp ispec)
            (princ ispec)
            (destructure-case ispec
              ((:value string id)  (format t ":value '~A' ~A" string id))
              ((:action string id) (format t ":action '~A' ~A" string id))))))
  (format t "Got a response back from the inspector~%")
  (format t "Title : ~A~%" title)
  (format t "  [type : ~A]~%" type)
  (dolist (ispec content)
    (print-ispec ispec))))

(defmethod set-package-response ((sliver sliver-console) package-name prompt-string)
 (format t "set-package-response ~A ~A~%" package-name prompt-string))

(defmethod new-features ((sliver sliver-console) features)
 (format t "new-features response ~A~%" features))

(defmethod decorated-arglist-response ((sliver sliver-console) result)
 (format t "Got decorated-arglist-response of ~A~%" result))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; testing below here
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar *sliver* nil)

(defun print-debug-level (debug-level)
 (with-slots (thread level condition restarts backtrace) debug-level
  (format t "New debugger level ~A~%" level)
  (format t "In thread ~A~%" thread)
  (format t "Condition is ~A~%" condition)
  (format t "Partial backtrace (~A entries):~%" (length backtrace))
  (dolist (frame backtrace)
   (format t "~A~%" frame))
  (format t "Restarts are~%")
  (loop :for n :from 1
        :for r :in restarts
        :do (format t "[~A] ~A~%" n r))))

(defun test-connect ()
 (unless *sliver* 
  (setf *sliver* (make-instance 'sliver-console :polling-mode nil))
  (connect *sliver*)))

(defun test-reset ()
 (when (and *sliver* (slot-value *sliver* 'swank-socket))
  (disconnect *sliver*))
 (setf *sliver* nil))

(defun test-get-connection-info ()
 (request-connection-info *sliver*))

(defun test-poll ()
 (poll-for-events *sliver*))

(defun test-compile (&optional load)
 (compile-file-send *sliver* (truename "test.lisp") load))

(defun test-notes ()
 (compile-notes-send *sliver*))

(defun test-eval-send ()
 (format t "Enter form to eval ~%")
 (eval-send *sliver* (read-line)))

(defun test-repl-eval-send ()
 (format t "Enter form to eval ~%")
 (repl-eval-send *sliver* (read-line)))

(defun test-set-package ()
 (format t "Enter package name~%")
 (set-package-send *sliver* (read-line)))

(defun test-interrupt ()
 (interrupt *sliver*))

(defun test-restart ()
 (let ((level (level-of (active-level *sliver*)))
        restart)
  (format t "Restart number?~%")
  (setf restart (parse-integer (read-line)))
  (invoke-restart-send *sliver* level restart)))

(defun test-inspect ()
 (format t "Enter form to inspect~%")
 (inspect-send *sliver* (read-line))
 (do-menu *inspect-menu*)
 (quit-inspector-send *sliver*))

(defun test-inspect-sub ()
 (format t "Enter subpart number to inspect~%")
 (inspect-nth-part-send *sliver* (parse-integer (read-line))))

(defun test-inspect-next () (inspector-next-send *sliver*))
(defun test-inspect-pop () (inspector-pop-send *sliver*))

(defun test-arglist ()
 (let (func index)
  (format t "Enter a function to get the arglist for~%")
  (setf func (read-line))
  (format t "Enter an argument index~%")
  (setf index  (ignore-errors (parse-integer (read-line))))
  (decorated-arglist-send *sliver* func index)))

(defparameter *debug-menu* `("Debug menu"
                             ("r" "Invoke a restart at this level" ,#'test-restart)
                             ("p" "Poll events" ,#'test-poll)
                            ))

(defparameter *inspect-menu* `("Inspect menu"
                              ("p" "Poll events" ,#'test-poll)
                              ("i" "Inspect a subpart" ,#'test-inspect-sub)
                              ("f" "Move forward in the inspect tree" ,#'test-inspect-next)
                              ("b" "Move backward in the inspect tree" ,#'test-inspect-pop)
                              ))

(defparameter *main-menu* `("Menu name"
                            ("c" "Connect" ,#'test-connect)
                            ("dis" "Reset (disconnect)" ,#'test-reset)
                            ("d" "Go to debugger menu" ,(lambda() (do-menu *debug-menu*)))
                            ("info" "Get Info" ,#'test-get-connection-info)
                            ("p" "Poll events" ,#'test-poll)
                            ("f" "Compile file 'test'" ,#'test-compile)
                            ("F" "Compile and load file 'test'" ,(lambda() (test-compile t)))
                            ("n" "Get notes from last compile" ,#'test-notes)
                            ("e" "Eval an expression" ,#'test-eval-send)
                            ("E" "Eval an expression as if on the REPL" ,#'test-repl-eval-send)
                            ("b" "Interrupt Swank" ,#'test-interrupt)
                            ("i" "Inspect a form" ,#'test-inspect)
                            ("p" "Set package" ,#'test-set-package)
                            ("a" "Get arglist for" ,#'test-arglist)
                            ))

;(delete-package :swank)
;(require 'sliver)
;(in-package :sliver)
;(do-menu *main-menu*)
(defun do-menu (menu)
 (when *sliver* (poll-for-events *sliver*))
 (loop :with selection = nil
       :until (equal "q" selection)
       :do
  (setf selection nil)
  (destructuring-bind (name &rest items) menu
   (format t "~%-- ~A --~%" name)
   (loop :for (key description func) :in items
         :do (format t " [~a] ~A~%" key description))
   (format t " [q] Quit~%")
   (setf selection (read-line))
   (loop :for (key description func) :in items
         :do (when (equal selection key)
               (funcall func)))))
 (format t "Leaving menu ~A~%" (car menu)))

(defun run ()
 (do-menu *main-menu*))

(export 'run)
