(defpackage :sliver
 (:use :cl)
 (:export 
  #:sliver
#:connect
#:disconnect
#:message-stream
#:swank-output-stream
#:poll-for-events

  ))

(defpackage :sliver-console
 (:use :cl :sliver))
