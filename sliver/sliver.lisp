(in-package :sliver)

(defvar *default-package* nil)

(defclass backend-swank-info ()
 ((swank-pid           :initform nil :documentation "The pid of the backend Lisp.")
  (lisp-features       :initform nil :documentation "*FEATURES* that the backend Lisp supports.")
  (communication-style :initform nil :documentation "Communication style used by the backend Lisp.")
  (lisp-package        :initform nil :documentation "The current package on the backend.")
  (lisp-package-prompt :initform nil :documentation "The current prompt string for the package on the backend.")
  (lisp-type           :initform nil :documentation "Backend implementation.")
  (lisp-version        :initform nil :documentation "Version of the backend.")
  (lisp-name           :initform nil :documentation "String name of the backend.")
  (host                :initform nil :documentation "Instance name for the backend.")
  (machine-instance    :initform nil :documentation "MACHINE-INSTANCE of the backend.")
  (machine-type        :initform nil :documentation "MACHINE-TYPE of the backend.")
  (protocol-version    :initform nil :documentation "The version running on the wire.")
  )
 (:documentation "Information about the backend Lisp"))

(defclass sliver ()
 ((connected-p         :initform nil :documentation "Socket connection to Swank is established")
  (swank-info          :initform nil :documentation "The latest information about the Swank backend.  May be NIL if the information has not yet arrived in from Swank.")
  (connected-fully-p   :initform nil :documentation "Managed to get connection info about Swank")
  (host                :initform nil :documentation "The host that Sliver is connected to")
  (polling-mode-p      :initform nil :initarg :polling-mode 
   :documentation "If true then the connection must be polled")
  (swank-socket        :initform nil :documentation "Socket used to communicate with Swank")
  (swank-stream        :initform nil :documentation "Stream interface to the Swank socket")
  (protocol-buffer     :initform (make-array 1024 :element-type 'base-char :fill-pointer 0 :adjustable t)
   :documentation "The accumulation buffer for messages coming in from Swank.")
  (swank-io-socket     :initform nil :documentation "Socket used for dedicated Swank io.  Ie, where *standard-output* would come out")
  (swank-io-stream     :initform nil :documentation "Stream interface to the dedicated Swank io socket.")
  (io-buffer           :initform nil
   :documentation "The accumulation buffer for dedicated IO messages, only created if there is a dedicated IO socket.")
  (continuation-counter :initform 0  :documentation "The number of continuations that have run, also cont ID.")
  (continuations       :initform '() :documentation "Pending closures that will be run when the appropriate response from Swank is received.")
  (swank-lisp-features :initform nil :documentation "Features of the backend Lisp.")
  (current-package     :initform nil :documentation "Current package that evaluation will be directed to.")
  (current-thread      :initform nil :documentation "Current thread to evaluate in.")
 )
 (:documentation "The base class for a Sliver connection.  This provides all of the basic functionality that a connection to Slime will need.  Each application that wants to integrate Sliver should inherit from this class and overide the abstract functions to allow seamless integration.  Many functions have two phases to them, a function is called on the Sliver instance, which passes information to Slime and returns.  Sometime later Slime will respond and a callback function will be invoked.  By default Sliver functions of the naming convention FOO-SEND will result in a callback named FOO-RESPONSE being called."))

(defgeneric connected-p (sliver)
 (:documentation "Return if this instance is connected to Swank."))

(defgeneric disconnect (sliver &optional terminate)
 (:documentation "Disconnect from the Swank backend, optionally terminating the backend Lisp."))

(defgeneric terminate-backend (sliver)
 (:documentation "A system specific way to kill the backend Lisp process. The default implementation
  does nothing."))

(defgeneric connect (sliver &optional host port)
 (:documentation "Connect to the Swank backend running on HOST at PORT.  If either HOST or PORT is NIL then Sliver will attempt to read the Swank config file and connect at those settings."))

(defgeneric poll-for-events (sliver)
 (:documentation "This function is used to poll the Swank backend for events."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; The exchanges that Sliver supports
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-exchange compile-file (filename &optional load)
                 `(swank:compile-file-for-emacs ,filename ,(if load t nil))
                 :documentation "Compile FILENAME on the backend Lisp, and optionally load it.  FILENAME must be correct for the remote Lisp, this is only an issue if Sliver is not running on the same host as Swank.")

(define-exchange compile-notes () '(swank:compiler-notes-for-emacs)
 :documentation "Get any compiler notes, warnings and errors from Swank for the last compilation.")

(define-exchange invoke-restart (debug-level restart)
                 `(swank:invoke-nth-restart-for-emacs ,debug-level ,restart)
 :documentation "Try to invoke the restart number at the debug level.  This is completely tied to the information obtained from a previous :DEBUG message.  Calls SWANK:INVOKE-NTH-RESTART-FOR-EMACS.")

(define-exchange compile-string (form-string &optional start buffer-name buffer-directory)
                 `(swank:compile-string-for-emacs ,form-string ,buffer-name ,start ,buffer-directory)
                 :documentation "Compile FORM-STRING on the Lisp backend.  The optional parameters should be used when compiling a substring from a file
START - the offset into the buffer that FORM-STRING starts at
BUFFER-NAME the name of the buffer that FORM-STRING is in
BUFFER-DIRECTORY the directory that BUFFER resides in.")

;;; INSPECTOR related exchanges
(defgeneric inspect-response (sliver title type content)
 (:documentation "The response function for almost all inspector activities"))
(defmethod inspect-response ((sliver sliver) title type content) nil)

(define-inspector-exchange inspect (form-to-inspect)
                           `(swank:init-inspector ,form-to-inspect)
                           :documentation "Inspect the given form")

(define-exchange quit-inspector ()
                 `(swank:quit-inspector)
                 :documentation "Inform Swank that we are done with the inspector")

(define-inspector-exchange inspect-nth-part (part-number)
                           `(swank:inspect-nth-part ,part-number)
                           :documentation "Inspect a sub part of the currently inspected object.")

(define-inspector-exchange inspector-pop ()
                           `(swank:inspector-pop)
                           :documentation "Pop back up a level in the inspector tree")

(define-inspector-exchange inspector-next ()
                           `(swank:inspector-next)
                           :documentation "Traverse down an inspect path that you have previously visited")
;;; Evaluation exchanges
(define-exchange eval (form-string)
                 `(swank:interactive-eval ,form-string)
                 :documentation "Evaluate FORM-STRING on the backend Lisp.")

(define-exchange repl-eval (form-string)
                 `(swank:listener-eval ,form-string)
                 :documentation "Evaluate FORM-STRING on the backend Lisp as if you evaluated it from the REPL.  This means that the normal REPL variables * ** ***, etc are bound as expected.")

(define-exchange set-package (package)
                 `(swank:set-package ,package)
                 :response-lambda (package-name prompt-string)
                 :custom-callback
                 (lambda (result) (destructuring-bind (package-name prompt-string) result
                                   (set-package-response sliver package-name prompt-string)))
                 :documentation "Sets the package on the backend Lisp, the return is the full name of the package and the string which can be printed at the REPL")

(defgeneric interrupt (sliver) (:documentation "Send an interrupt to the the Swank backend"))

;;; Editing helper exchanges
(define-exchange decorated-arglist (function &optional index)
                 (append `(swank:arglist-for-echo-area '(,function)) (when index `(:arg-indices '(,index))))
                 :documentation "Returns a decorated argument list for FUNCTION.  ")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functions below here have default implementations, but almost all implementations that use Sliver
;;; will need to specialize them
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric open-dedicated-stream (sliver port)
 (:documentation "Swank may request that we use a dedicated port for streams output, this function is called if that request is made.  The Swank message :OPEN-DEDICATED-OUTPUT-STREAM triggers this function."))

(defgeneric handle-indentation-update (sliver indent-info)
 (:documentation "Called when Swank sends the message :INDENTATION-UPDATE.  INDENT-INFO is a list of (macro-name . spaces-to-indent).  The default function does nothing.  TODO - is spaces-to-indent what the CDR really is?  The Swank message :INDENTATION-UPDATE triggers this function."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functions below here are abstract, and must be supplied by children.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric message (sliver control-string &rest args)
 (:documentation "Abstract.  The same as FORMAT, but the first parameter is the class instance rather than a stream."))

(defgeneric swank-output (sliver string)
 (:documentation "Abstract.  Messages from the backend Lisp that would normally go out *standard-output* will be sent to this stream."))

(defgeneric debug-message (sliver control-string &rest args)
 (:documentation "Abstract.  The same as FORMAT, but the first parameter is the class instance rather than a stream.  Debugging messages will be sent to this function."))

(defgeneric init-debugger-level (sliver thread level condition restarts frames)
 (:documentation "Abstract.  This function is called when the backend Lisp traps an error.  
It is possible to have multiple levels of debugging active.
CONDITION is a string describing the condition to debug.
RESTARTS is a list of strings (NAME DESCRIPTION) for each available restart.
FRAMES is a list (NUMBER DESCRIPTION) describing the initial
portion of the backtrace. Frames are numbered from 0.
Slime normally dispatches a list of continuations to this function, however I'm not sure what they do or really what a non-Slime frontend would do with them.
The Swank message :DEBUG triggers this function."))

(defgeneric activate-debugger-level (sliver thread level)
 (:documentation "Abstract.  Called directly after START-DEBUGGER to activate a particular debugging level.  ??May be called to move between debugging levels??  The Swank message :DEBUG-ACTIVATE triggers this function."))

(defgeneric exit-debugger (sliver thread level stepping)
 (:documentation "Abstract.  Called when the Swank message :DEBUG-RETURN is received.  LEVEL indicates the debugger level that has just finished up."))

(defgeneric new-features (sliver features)
 (:documentation "Abstract.  When new values are pushed to Swank's *FEATURES* list, this function will be called."))

;;; More commands to handle
#|
DONE -- ((:emacs-interrupt thread)
        ((:emacs-return-string thread tag string)
        ((:emacs-return thread tag value)

DONE -- (swank:connection-info)
DONE -- (list 'swank:listener-eval string)
DONE -- (swank:set-package ,package)
DONE -- (swank:compile-file-for-emacs ,lisp-filename ,(if load t nil) ,@(if (local-variable-p 'slime-coding (current-buffer)) (list (slime-coding-system-cl-name slime-coding))))
DONE  - (swank:compile-string-for-emacs ,string ,(buffer-name) ,start-offset ,(if (buffer-file-name) (file-name-directory (buffer-file-name))))
DONE -- (swank:compiler-notes-for-emacs)
DONE -- (swank:interactive-eval ,string)
DONE -- (swank:set-package ,package)
DONE -- (list 'swank:invoke-nth-restart-for-emacs sldb-level restart)
DONE -- (swank:init-inspector ,form ,(not no-reset))
DONE -- (swank:quit-inspector)
DONE -- (swank:inspect-nth-part ,part-number)
DONE -- (swank:inspector-pop)
DONE -- (swank:inspector-next)
BORING  (swank:describe-inspectee)
        (swank:inspect-in-frame ,string ,number)
        (swank:inspect-current-condition)
        (swank:inspect-frame-var ,frame ,var)
        (swank::inspector-call-nth-action ,action-number)
BORING  (swank:inspector-nth-part ,number)
        (swank:pprint-inspector-part ,part)
        (swank:inspector-reinspect)
DONE -- (swank:arglist-for-echo-area (quote (,name)))
NEED ?? (swank:arglist-for-insertion ',name)
        (swank:list-all-package-names t)
        (swank:load-file-set-package ,filename ,package)
BORING  (swank:io-speed-test 5000 1)
        (swank::describe-to-string (swank::lookup-presented-object ',(slime-presentation-id presentation)))
        (swank::swank-pprint (cl:list (swank::lookup-presented-object ',(slime-presentation-id presentation))))
        (swank::menu-choices-for-presentation-id ',what)
        (swank::execute-menu-choice-for-presentation-id ',what ,nchoice ,(nth (1- nchoice) choices))
HUH ??  (swank:get-repl-result '%s)
        (swank:clear-repl-results)
        (swank:simple-break)
        (swank:default-directory)
        (swank:compile-file-if-needed ,(slime-to-lisp-filename filename) t)
        (swank:quit-lisp)
        (swank:list-all-systems-in-central-registry)
        (swank:complete-form ,form-string)
        (swank:variable-desc-for-echo-area ,global)
        (swank:completions ,prefix ',(slime-current-package))
        (swank:simple-completions ,prefix ',(slime-current-package))
        (swank:completions-for-keyword ',operator-designator ,prefix ',arg-indices)
        (swank:completions-for-character ,prefix)
        (swank:fuzzy-completions ,prefix ,(or default-package (slime-find-buffer-package) (slime-current-package)) :limit ,slime-fuzzy-completion-limit :time-limit-in-msec ,slime-fuzzy-completion-time-limit-in-msec)
        (swank:fuzzy-completion-selected ,no-properties ',completion)
        (swank:find-definitions-for-emacs ,name)
        (swank:buffer-first-change ,filename)
        (swank:eval-and-grab-output ,string)
        (swank:interactive-eval-region ,(buffer-substring-no-properties start end))
        (swank:re-evaluate-defvar ,form)
        (swank:pprint-eval ,(slime-last-expression))
        (swank:value-for-editing ,form-string)
        (swank:commit-edited-value ,slime-edit-form-string ,value)
        (swank:untrace-all)
        (swank:swank-toggle-trace ,spec)
        (swank:disassemble-symbol ,symbol-name)
        (swank:undefine-function ,symbol-name)
        (swank:load-file ,lisp-filename)
        (swank:toggle-profile-fdefinition ,fname-string)
        (swank:unprofile-all)
        (swank:profile-report)
        (swank:profile-reset)
        (swank:profiled-functions)
        (swank:profile-package ,package ,callers ,methods)
        (swank:describe-symbol ,symbol-name)
        (swank:documentation-symbol ,symbol-name "(not documented)")
        (swank:describe-function ,symbol-name)
        (swank:apropos-list-for-emacs ,string ,only-external-p ,case-sensitive-p ',package)
        (swank:describe-definition-for-emacs ,item ,type)
        (swank:xref ',type ',symbol)
        (swank:set-default-directory ,(slime-to-lisp-filename directory))
        (swank:debugger-info-for-emacs 0 10)
        (swank:backtrace ,start ,end)
        (swank:frame-source-location-for-emacs ,frame-number)
        (swank:eval-string-in-frame ,string ,number)
        (swank:pprint-eval-string-in-frame ,string ,number)
        (swank:frame-locals-for-emacs ,frame)
        (swank:frame-catch-tags-for-emacs ,frame)
        (swank:backtrace ,(1+ last) nil)
        (swank:throw-to-toplevel)
        (swank:sldb-continue)
        (swank:sldb-abort)
        (swank:sldb-break-with-default-debugger)
        (swank:sldb-step ,frame)
        (swank:sldb-next ,frame)
        (swank:sldb-out ,frame)
        (swank:sldb-break-on-return ,frame)
        (swank:sldb-break ,name)
        (swank:sldb-disassemble ,frame)
        (list 'swank:sldb-return-from-frame number string)
        (list 'swank:restart-frame number)
        (swank:list-threads)
        (swank:quit-thread-browser)
        (swank:kill-nth-thread ,id)
        (swank:start-swank-server-in-thread ,id ,file)
        (swank:debug-nth-thread ,id)
        (swank:mop :subclasses ,name)
        (swank:xref ,type ,name)
        (swank:update-indentation-information)
        (swank::read-from-emacs "CL-USER")
        (swank:start-server "CL-USER")
        (swank:list-all-package-names t)
        (swank:load-file-set-package ,filename ,package)
|#
