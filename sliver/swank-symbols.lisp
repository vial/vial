(unless (find-package :swank)
 (format t "Package :swank doesn't exist~%")
 (defpackage :swank
  (:use :common-lisp)
  (:export
 ; Init/misc stuff
              :*swank-io-package*
              :connection-info
              :arglist-for-echo-area
              :swank-macroexpand-1
              :swank-macroexpand-all
 ; eval/compile
              :listener-eval
              :interactive-eval
              :compile-file-for-emacs
              :re-evaluate-defvar
              :compile-string-for-emacs
              :compiler-notes-for-emacs
 ; SLDB
              :invoke-nth-restart-for-emacs
              :throw-to-toplevel
              :debugger-info-for-emacs
              :frame-locals-for-emacs
 ; Inspector
              :init-inspector
              :inspect-nth-part
              :inspector-pop
              :quit-inspector)))
