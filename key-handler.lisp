;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(define-condition no-more-keys () ()
 (:report (lambda (condition stream) (format stream "No more chars in the key buffer~%"))))

(define-condition abort-command () ()
 (:report (lambda (condition stream) (format stream "Command was aborted with \Cc or ESC~%"))))

(defparameter *total-key-count* 0)
(defparameter *key-buffer* "")
(defvar *key-pos* 0)
(defvar *key-pos-max* 0)
(defvar *last-command* nil)

(defun number-of-keys (keys)
 (loop :for count  :from 0
       :for start = 0 :then (incf start (bytes-in-front-key (subseq keys start)))
       :while (< start (length keys))
       :finally (return count)))

(defun bytes-in-front-key (keys)
 (unless (zerop (length keys))
  (if (char= #\< (aref keys 0)) 
    (1+ (position #\> keys))
   1)))

(defun bytes-in-current-key ()
 (unless (zerop (length *key-buffer*))
  (bytes-in-front-key (subseq *key-buffer* *key-pos*))))

(defun get-key ()
 (when (or (zerop (length *key-buffer*)) (>= *key-pos* (length *key-buffer*)))
  (signal 'no-more-keys))
 ;(format t "~A : ~A ~A" *key-buffer* *key-pos* (bytes-in-current-key))
 (subseq *key-buffer* *key-pos* (+ *key-pos* (bytes-in-current-key))))

(defun advance-by-key (&optional (num-keys 1))
 (let ((len (length *key-buffer*))
       (bytes-advanced 0))
  (loop :repeat num-keys
        :while (< *key-pos* len)
        :do (let ((bytes (bytes-in-current-key)))
            (incf *key-pos* bytes)
            (incf bytes-advanced bytes)))
  (when (> *key-pos* *key-pos-max*)
   (setf *key-pos-max* *key-pos*))
  bytes-advanced))

(defun finished-command-processing () nil)

(defun remove-command-from-buffer ()
 (format t "Processing command(remove1): ~A ~A ~A~%" *key-buffer* *key-pos* *key-pos-max*)
 (setf *key-buffer* (subseq *key-buffer* *key-pos-max*)
  *key-pos* 0
  *key-pos-max* 0)
 (format t "Processing command(remove2): ~A ~A ~A~%" *key-buffer* *key-pos* *key-pos-max*)
)

;------------------- MODE HANDLING AND REMAPPING --
(defvar *mode* 'normal
 "Mode may be 'normal, 'insert, or 'ex")

; The remapping tables for each mode
; each table has an escaped string as the key, and a string as
; the value.  When key is pressed, the value (if there is one)
; is inserted into the key-buffer
(defvar *normal-remap* (make-hash-table :test #'equal))
(defvar *insert-remap* (make-hash-table :test #'equal))
(defvar *search-remap* (make-hash-table :test #'equal))

; The ex-remap table is different from the above in that it the key can be
; any string value, and is effectively a way to abbreviate ex commands
; ie bn => bnext
(defvar *ex-remap* (make-hash-table :test #'equal))

(defun get-mode-table (&optional (mode *mode*))
 (ecase mode
  (search *search-remap*)
  (normal *normal-remap*)
  (insert *insert-remap*)
  (ex     *ex-remap*)))

(defun map-key (key)
 (let* ((table (get-mode-table))
        (value (gethash key table)))
  (if value
   value
   key)))

(defun get-key-from-keyboard ()
 (let ((key (get-key-from-io-layer)))
  (when key
   (run-hook (buffer-of *active-cursor*) 'key-pressed key)
   (incf *total-key-count*)
   (setf *key-buffer* (concatenate 'string *key-buffer* (string (map-key key)))))))

(defun define-key-remap (mode key remap)
 (setf (gethash key (get-mode-table mode)) remap))

(define-condition leaving-simple-input-mode () ()
 (:report (lambda (condition stream) (format stream "Leaving insert mode~%"))))

;; TODO change this to leave-simple-input-mode
(defun leave-simple-input-mode ()
 (signal 'leaving-simple-input-mode))

(defun simple-input-loop (regular-key-func special-key-func)
 (assert (not (eq *mode* 'normal)))
 (render *active-cursor*)
 (let ((start-pos *key-pos*))
  (format t "Processing insert ~A~%" *key-buffer*)
  (handler-case
  (loop 
   (loop until (or (zerop (length *key-buffer*)) (>= *key-pos* (length *key-buffer*)))
    do (let ((key (get-key)))
        (cond ((< 1 (length key)) (funcall special-key-func key))
              (t                  (funcall regular-key-func key)))
        (advance-by-key)))
   (render *active-cursor*)
   ;(get-key-from-keyboard))
   (wait-for-event))
  (leaving-simple-input-mode (c) 
   (format t "Got signal ~A~%" c)
   (advance-by-key 1)
   (setf *key-pos* start-pos)
    (finished-command-processing)))))

#|
;;;; OBSOLETE CODE

(defmacro with-next-keys (keys-to-move &body body)
 (let ((bytes-to-backtrack (gensym)))
  `(let ((,bytes-to-backtrack (advance-by-key ,keys-to-move)))
      ,@body
      (decf *key-pos* ,bytes-to-backtrack))))

(defun get-num-and-advance ()
 (if (char= #\0 (elt *key-buffer* *key-pos*))
  (values 0 1)
  (multiple-value-bind (int pos)
   (parse-integer *key-buffer* :start *key-pos* :junk-allowed t)
   (values int (- pos *key-pos*)))))

(defun signal-abort-command ()
 (with-next-keys 1 nil)
 (signal 'abort-command))

(defun do-command ()
 (format t "Processing command: ~A~%" *key-buffer*)
 (setf *active-register* (new-cursor-and-buffer))
 (with-undo-block *active-cursor*
  (handler-case
   (progn
    (do-key)
    (remove-command-from-buffer)
    (push-active-register-to-ring))
   (no-more-keys (c) (format t "Got signal ~A~%" c)
    (setf *key-pos-max* 0
     *key-pos* 0))
   (abort-command (c) (format t "Got signal ~A~%" c)
    (remove-command-from-buffer)))))

(defun do-key ()
 (let ((key (get-key)))
  (cond ((symbolp      key) (do-special))
   ((digit-char-p key) (do-digit))
   (t (do-alpha)))))

(defun do-special ()
 (let ((modifier (get-key)))
  (with-next-keys 1
   (let ((key (get-key)))
    (if (and (eq 'CTRL modifier) (char-equal #\c key))
     (signal-abort-command)
     (with-next-keys 1
      (do-key)))))))

(defun do-digit ()
 (multiple-value-bind (num advance) (get-num-and-advance)
  (format t "Numbers are is ~A~%" num)
  (when (zerop num)  ; handle the special case of 0 being pressed
   (cursor-move-by *active-cursor* 0 1 :col-absolute t)
   (with-next-keys 1
    (finished-command-processing)))
  (dotimes (i num)
   (with-next-keys advance
    (do-key)))))

(defun do-alpha ()
 (let* ((key  (get-key))
        (func (gethash key *command-functions*)))
 (format t "alpha is ~A ~A~%" key func)
  (cond 
   (func
    (with-next-keys 1
     (funcall func)))
   (t (with-next-keys 1
       (do-key))))))

|#
