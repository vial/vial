;;; This software is free under the zlib license, see LICENSE for details

; This whole module is pretty damn UGLY
(in-package :vial)

(defparameter *search-cursor* nil)
(defparameter *pre-search-cursor* nil)
(defparameter *current-search* nil)

(defun enter-search-mode ()
 (unless *search-cursor* (setf *search-cursor* (new-cursor-and-buffer)))
 (format t "Entering search mode~%")
 (let ((*pre-search-cursor* *active-cursor*))
  (let* ((*mode* 'search)
         (*active-cursor* *search-cursor*))
   (show-cursor-in-win *search-cursor* *status-window*)
   (insert-line)
   (insert-string "/")
   (simple-input-loop #'insert-normal-key #'search-special-key))))

(defun search-special-key (key)
 (format t "do-ex-key-special ~A~%" key)
 (format t "    ~A~%" (get-key))
 (cond
  ((string-equal key "<TAB>"))
  ((string-equal key "<C-C>") (abort-search-mode))  ; \Cc
  ((string-equal key "<ESC>") (abort-search-mode))  ; Escape
  ((string-equal key "<CR>") (process-current-search) (abort-search-mode)) ; \Cj or return
  ((string-equal key "<Backspace>") (delete-chars 1) 
                                    (when (zerop (col-of *active-cursor*)) 
                                     (abort-search-mode)))
 )
(advance-by-key))

(defun process-current-search ()
 (let ((line (text-at-point)))
  (format t "Search for ~A ~A~%" (subseq line 1) *pre-search-cursor*)
  (setf *current-search* (subseq line 1))
  (search-forward-highlight-all *pre-search-cursor* (subseq line 1))))

(defun redo-search-forward ()
 (let ((buffer (buffer-of *active-cursor*)))
  (if (eq (current-search-mark-of buffer) (first (backward (search-marks-of buffer))))
   (setf (current-search-mark-of buffer) (first (forward (search-marks-of buffer))))
   (setf (current-search-mark-of buffer) (node-after (current-search-mark-of buffer))))
  (cursor-move-to-mark *active-cursor* (current-search-mark-of buffer))))

(defun redo-search-backward ()
 (let ((buffer (buffer-of *active-cursor*)))
  (if (eq (current-search-mark-of buffer) (first (forward (search-marks-of buffer))))
   (setf (current-search-mark-of buffer) (first (backward (search-marks-of buffer))))
   (setf (current-search-mark-of buffer) (node-before (current-search-mark-of buffer))))
  (cursor-move-to-mark *active-cursor* (current-search-mark-of buffer))))

(defun abort-search-mode ()
 (leave-simple-input-mode))

(defun search-handle-line-changed (cursor old-text)
 (declare (ignore old-text))
 )

(defun search-forward-highlight-all (cursor regex)
 ; basically, scan and mark every occurance of REGEX, starting from CURSOR
 ; this code is a bit ugly because of the need to scan the first line
 ; starting at the cursor column, then when we scan the first line again (when it is the
 ; last line), we need to just scan the front part of it
 (let ((scanner       (if (stringp regex) (regex-create-scanner regex :case-insensitive-mode t) regex))
       (cursor-col    (col-of cursor))
       (first-mark    nil)
       (marks         (make-instance 'dlist)))
  (flet ((scan-line (cursor start end)
          (let* ((line  (line-of cursor))
                 (text  (slot-value line 'text)))
           (loop :for match :on (regex-all-matches scanner text :start start) :by #'cddr
                 :do (let ((start (first match))
                           (end   (second match)))
                     (when (and first-mark
                                (eq (line-of first-mark) (line-of cursor))
                                (=  (col-of first-mark) (col-of cursor)))
                      ; we've run into our first mark again
                      (return-from scan-line t))
                     (set-style-in-line line 'search (make-style 'WHITE 'BLUE '(NORMAL)) start end)
                     (let ((mark (make-instance 'mark :line (line-of cursor) :col start)))
                      (unless first-mark
                       (setf first-mark mark))
                      (dappend mark marks))))
           nil)))
   (remove-highlights cursor)
   (do-from-cursor-to-cursor cursor #'scan-line)
   (setf (search-marks-of (buffer-of cursor)) marks
         (current-search-mark-of (buffer-of cursor)) (first (forward marks)))
   (when (current-search-mark-of (buffer-of cursor))
    (cursor-move-to-mark cursor (current-search-mark-of (buffer-of cursor)))))))

(defun re-search-forward (cursor regex)
 (let ((scanner (if (stringp regex) (regex-create-scanner regex) regex)))
  (flet ((re-search-this-line (cursor)
          (multiple-value-bind (start end) (regex-scan scanner (get-text-line-at-point cursor) :start (col-of cursor))
           ;(format t "Scanning |~A| Match ~A:~A~%" (subseq (get-text-line-at-point cursor) (col-of cursor)) start end)
           (when (and start end)
            (cursor-move-by cursor 0 end :col-absolute t)
            (make-instance 'mark :line-num (line-num-of cursor) :col start)))))
   ;(format t "re-search-forward for ~A~%" regex)
   (do-from-cursor-to-cursor cursor #'re-search-this-line))))

(defun remove-highlights (cursor)
  (when (search-marks-of (buffer-of cursor))
    (dolist (m (forward (search-marks-of (buffer-of cursor))))
      (remove-layer (line-of m) 'search))))

(defun clear-current-search (cursor)
  (remove-highlights cursor)
  (setf *current-search* nil))
    
