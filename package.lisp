;;; This software is free under the zlib license, see LICENSE for details

(defpackage :vial
 (:use :cl :cl-fad :sliver)
 (:export vial))

(in-package :vial)

(defun length1-p (list)
 "Is the length of this list exactly 1?"
 (cond
  ((null list) nil)      ; null list is false
  ((null (rest list)) t) ; null rest of the list, length is 1
  (t nil)))              ; otherwise false
