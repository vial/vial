;;; This software is free under the zlib license, see LICENSE for details

;; This file exercises and tests VIAL
(defvar *test-active* t)
;(load "vial")

(defvar *all-tests* ())

(defmacro def-test (name args &body body)
 `(progn
     (defun ,name ,args
      (let ((*active-cursor* (new-cursor-and-buffer)))
      (format t "Testing ~A~%" ',name)
      ,@body
      (format t "Done Testing ~A~%" ',name)
      ))
     (push #',name *all-tests*)))

(defun run-all-tests ()
 (dolist (test *all-tests*)
  (funcall test)))
#|
    (run-all-tests)
|#
(defun get-key-from-keyboard ()
 (when (zerop (length *key-buffer*)) (setf *vial-running* nil))
  nil)

(defun render (view))
(defun init-io-layer ())
(defun cleanup-io-layer())

(defun do-test-io (input)
 (setf *key-buffer* input
       *key-pos-max* 0
       *key-pos* 0)
 (main-loop))

(defun compare-to (filename)
 (let ((filename (concatenate 'string "./regression-tests/" filename)))
  (with-open-file (file filename :direction :input)
   (loop for fline = (read-line file nil 'eof) then (read-line file nil 'eof)
         for blines = (slot-value (buffer-of *active-cursor*) 'lines) then (rest blines)
         for bline = (first blines) then (first blines)
         for line-num = 1 then (incf line-num)
         while (and bline (not (eq fline 'eof)))
         do
         (unless (string= fline (slot-value bline 'text))
          (format t "Lines differ on line ~A ~A != ~A~%" line-num fline (slot-value bline 'text)))))))

(defun undo-redo-compare-to (filename)
 (compare-to filename)
 (loop while (apply-undo *active-cursor*))
 (loop while (apply-redo *active-cursor*))
 (compare-to filename))

(def-test insert-basic ()
  (do-test-io "2itimes the same thing (\\\\)\\Cc")
  (undo-redo-compare-to "insert-basic.data"))

(def-test insert-o ()
 (do-test-io "ione\\CcOtwo\\Ccothree\\Cc0li\\Cj\\Cc")
 (undo-redo-compare-to "insert-o.data"))

(def-test special-keys ()
 (do-test-io "ithisisasingletext\\Cc04li\\Cj\\\\ \\Cc4li\\Ch\\C{")
 (undo-redo-compare-to "special-keys.data"))

