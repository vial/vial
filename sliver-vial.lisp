(in-package :vial)

(defclass sliver-vial (sliver)
 ((level :initform 0 :accessor level)))

(defmethod message ((sliver sliver-vial) control-string &rest args)
 (apply #'format (append (list t control-string) args)))

(defmethod swank-output ((sliver sliver-vial) string)
 (write-string "SO:" *standard-output*)
 (write-string string *standard-output*))

(defmethod debug-message ((sliver sliver-vial) control-string &rest args)
 (apply #'format (nconc (list t control-string) args)))

(defmethod compile-file-response ((sliver sliver-vial) result)
 (format t "compile-file-response got back ~A~%" result))

(defmethod get-compile-notes-response ((sliver sliver-vial) result)
 (format t "get-compile-notes got back ~A~%" result))

(defmethod eval-response ((sliver sliver-vial) result)
 (status-message (format nil "~A" result)))

(defparameter *debug-buffer* nil)
(defun show-debug-buffer ()
 (unless *debug-buffer* 
  (setf *debug-buffer* (new-cursor-and-buffer))
  (push *debug-buffer* *cursor-list*))
 (let ((win (find-window-of-cursor *active-cursor*)))
  (setf *active-cursor* *debug-buffer*)
  (show-cursor-in-win *active-cursor* win)))

(defmethod init-debugger-level ((sliver sliver-vial) thread level condition restarts frames)
 (show-debug-buffer)
 (let ((*standard-output* *debug-buffer*))
  (format t "Debugger has been started~%")
  (format t "thread ~A~%" thread)
  (format t "level ~A~%" level)
  (setf (level *sliver*) level)
  (format t "condition ~A~%" condition)
  (format t "frames ~A~%" frames)
  (format t "Restarts (at level ~A)~%" level)
  (loop :for i :from 1
   :for restart :in restarts
   :do 
   (format t "[~A] ~A~%" i restart))))

(defmethod activate-debugger-level ((sliver sliver-vial) thread level)
 (format t "Activating debugger level~%")
 (format t "Thread ~A~%" thread)
 (format t "Level ~A~%" level))

(defmethod exit-debugger ((sliver sliver-vial) thread level stepping)
 (format t "Exiting debugger level ~A~%" level))

(defmethod inspect-response ((sliver sliver-vial) title type content)
 (flet ((print-ispec (ispec)
          (if (stringp ispec)
            (princ ispec)
            (destructure-case ispec
              ((:value string id)  (format t ":value '~A' ~A" string id))
              ((:action string id) (format t ":action '~A' ~A" string id))))))
  (format t "Got a response back from the inspector~%")
  (format t "Title : ~A~%" title)
  (format t "  [type : ~A]~%" type)
  (dolist (ispec content)
    (print-ispec ispec))))

;;; Vial functions to manipulate *SWANK*
(defvar *sliver* nil)

(defun connect-to-swank ()
 (unless *sliver*
  (setf *sliver* (make-instance 'sliver-vial))
  (connect *sliver* "localhost" 4015)))

(defun eval-form ()
 (when *sliver*
  (let ((region (match-pair *active-cursor* #\( #\))))
   (when region
    (let ((str (region->string region)))
     (format t "Region is ~%~A~%" str)
     (eval-send *sliver* str))))))

(define-pattern 'op ",c" #'connect-to-swank)
(define-pattern 'op ",x" #'eval-form)
(define-pattern 'op ",p" (lambda () (poll-for-events *sliver*)))
