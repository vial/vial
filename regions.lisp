;;; This software is free under the zlib license, see LICENSE for details

(in-package :vial)

(defclass region ()
 ((cursor1 :initarg :cursor1 :initform nil :accessor cursor1-of)
  (cursor2 :initarg :cursor2 :initform nil :accessor cursor2-of)))

(defmethod initialize-instance :after ((r region) &key)
 (when (and (cursor1-of r) (cursor2-of r))
  (ensure-region-order r)))

(defmacro with-region (&body body)
 (let ((region (gensym)))
  `(let ((,region (start-region)))
      ,@body
      (ensure-region-order (stop-region ,region)))))

(defun adjust-cursor (target source)
 (setf (line-num-of  target) (line-num-of source)
       (line-of      target) (line-of source)
       (line-list-of target) (line-list-of source)
       (col-of       target) (col-of source)))

(defun start-region (&optional (cursor *active-cursor*))
 (let ((region (make-instance 'region)))
  (setf (cursor1-of region) (make-cursor-copy cursor))
  region))

(defun stop-region (region &optional (cursor *active-cursor*))
 (setf (cursor2-of region) (make-cursor-copy cursor))
 region)

(defun swap-region-ends (region)
 (let ((temp (cursor1-of region)))
  (setf (cursor1-of region) (cursor2-of region)
        (cursor2-of region) temp))
 region)

(defun ensure-region-order (region)
 (let ((c1 (cursor1-of region))
       (c2 (cursor2-of region)))
 (if (or (> (line-num-of c1) (line-num-of c2))
           (and (= (line-num-of c1) (line-num-of c2))
                (> (col-of c1) (col-of c2))))
  (swap-region-ends region)
  region)))

(defun delete-region (region &key (whole-lines nil))
 (let ((c1 (cursor1-of region))
       (c2 (cursor2-of region)))
 (assert (eq (buffer-of c1) (buffer-of c2)))
 (adjust-cursor *active-cursor* c1)
 (cond
  ((eq (line-num-of c1) (line-num-of c2)) (delete-chars (- (col-of c2) (col-of c1)) :cursor c1 :to-right t))
  (whole-lines
   (loop :for line :from (line-num-of c1) :to (line-num-of c2)
         :do
         (delete-single-line-from-active)))
  (t
   (delete-chars (1+ (- (length (get-text-line-at-point c1)) (col-of c1))) :cursor c1 :to-right t)
   (delete-chars (col-of c2) :cursor c2)
   (cursor-move-by *active-cursor* 1 0)
   (loop :for line :from (1+ (line-num-of c1)) :to (1- (line-num-of c2))
         :do
         (delete-single-line-from-active))
   (cursor-move-by *active-cursor* -1 0))
  )))

(defun pcursor (c name)
 (format t "Name ~A line-num ~A col ~A~%" name (line-num-of c) (col-of c)))

(defun cursor-in-region-p (cursor region)
 (let* ((c1 (cursor1-of region))
        (c2 (cursor2-of region))
        (line (line-num-of cursor))
        (line1 (line-num-of c1))
        (line2 (line-num-of c2)))
 (pcursor c1 "c1")
 (pcursor cursor "cursor")
 (pcursor c2 "c2")
 (let ((ret 
 (and (eq (buffer-of c1) (buffer-of cursor))     ; same buffer
      (cond 
        ((< line1 line line2) (format t "1~%") t) ; within the lines
        ; on the same line
        ((and (= line1 line) (= line2 line)) (format t "2~%") (<= (col-of c1) (col-of cursor) (col-of c2)))
        ; or, cursor is on a start/end line, and within the col region
        ((= line1 line) (<= (col-of c1) (col-of cursor)) (format t "3~%") t)
        ((= line2 line) (<= (col-of cursor) (col-of c2)) (format t "4~%") t)))))
(format t "ret is ~A~%" ret)
ret)))

(defun region->string (region)
 (let* ((c1         (cursor1-of region))
        (c2         (cursor2-of region))
        (col1       (col-of c1))
        (col2       (col-of c2))
        (first-line (line-of c1))
        (last-line (line-of c2)))
  (with-output-to-string (str)
    (if (eq first-line last-line)
      (write-string (slot-value first-line 'text) str :start col1 :end (1+ col2))
      (loop :for lines :on (line-list-of (cursor1-of region))
            :for line = (first lines)
            :do
            (cond
             ((eq line first-line) (write-line (slot-value line 'text) str :start col1))
             ((eq line last-line) (write-string (slot-value line 'text) str :end (1+ col2)))
             (t (write-line (slot-value line 'text) str)))
            :until (eq line last-line))))))
