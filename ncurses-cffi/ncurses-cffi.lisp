(in-package :ncurses)

;;;SWIG wrapper code starts here

(defmacro defanonenum (&body enums)
   "Converts anonymous enums to defconstants."
  `(progn ,@(loop for value in enums
                  for index = 0 then (1+ index)
                  when (listp value) do (setf index (second value)
                                              value (first value))
                  collect `(defconstant ,value ,index))))

;;;SWIG wrapper code ends here


(defcvar ("COLORS" COLORS)
 :int)

(defcvar ("COLOR_PAIRS" COLOR_PAIRS)
 :int)

(defconstant COLOR_BLACK 0)

(defconstant COLOR_RED 1)

(defconstant COLOR_GREEN 2)

(defconstant COLOR_YELLOW 3)

(defconstant COLOR_BLUE 4)

(defconstant COLOR_MAGENTA 5)

(defconstant COLOR_CYAN 6)

(defconstant COLOR_WHITE 7)

(defcstruct cchar_t
	(attr :pointer)
	(chars :pointer))

(defcstruct _win_st
	(_cury :pointer)
	(_curx :pointer)
	(_maxy :pointer)
	(_maxx :pointer)
	(_begy :pointer)
	(_begx :pointer)
	(_flags :short)
	(_attrs :pointer)
	(_bkgd :int)
	(_notimeout :int)
	(_clear :int)
	(_leaveok :int)
	(_scroll :int)
	(_idlok :int)
	(_idcok :int)
	(_immed :int)
	(_sync :int)
	(_use_keypad :int)
	(_delay :int)
	(_line :pointer)
	(_regtop :pointer)
	(_regbottom :pointer)
	(_parx :int)
	(_pary :int)
	(_parent :pointer)
	(_yoffset :pointer)
	(_pad :pointer))

(defcstruct _win_st__pad
	(_pad_y :pointer)
	(_pad_x :pointer)
	(_pad_top :pointer)
	(_pad_left :pointer)
	(_pad_bottom :pointer)
	(_pad_right :pointer))

(defcfun ("curses_version" curses_version) :string)

(defcfun ("addch" addch) :int
  (arg0 :int))

(defcfun ("addchnstr" addchnstr) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("addchstr" addchstr) :int
  (arg0 :pointer))

(defcfun ("addnstr" addnstr) :int
  (arg0 :string)
  (arg1 :int))

(defcfun ("addstr" addstr) :int
  (arg0 :string))

(defcfun ("attroff" attroff) :int
  (arg0 :int))

(defcfun ("attron" attron) :int
  (arg0 :int))

(defcfun ("attrset" attrset) :int
  (arg0 :int))

(defcfun ("attr_get" attr_get) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfun ("attr_off" attr_off) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("attr_on" attr_on) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("attr_set" attr_set) :int
  (arg0 :pointer)
  (arg1 :short)
  (arg2 :pointer))

(defcfun ("baudrate" baudrate) :int)

(defcfun ("beep" beep) :int)

(defcfun ("bkgd" bkgd) :int
  (arg0 :int))

(defcfun ("bkgdset" bkgdset) :void
  (arg0 :int))

(defcfun ("border" border) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int))

(defcfun ("box" box) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("can_change_color" can_change_color) :int)

(defcfun ("cbreak" cbreak) :int)

(defcfun ("chgat" chgat) :int
  (arg0 :int)
  (arg1 :pointer)
  (arg2 :short)
  (arg3 :pointer))

(defcfun ("clearok" clearok) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("clrtobot" clrtobot) :int)

(defcfun ("clrtoeol" clrtoeol) :int)

(defcfun ("color_content" color_content) :int
  (arg0 :short)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfun ("color_set" color_set) :int
  (arg0 :short)
  (arg1 :pointer))

(defcfun ("COLOR_PAIR" COLOR_PAIR) :int
  (arg0 :int))

(defcfun ("copywin" copywin) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfun ("curs_set" curs_set) :int
  (arg0 :int))

(defcfun ("def_prog_mode" def_prog_mode) :int)

(defcfun ("def_shell_mode" def_shell_mode) :int)

(defcfun ("delay_output" delay_output) :int
  (arg0 :int))

(defcfun ("delch" delch) :int)

(defcfun ("delscreen" delscreen) :void
  (arg0 :pointer))

(defcfun ("delwin" delwin) :int
  (arg0 :pointer))

(defcfun ("deleteln" deleteln) :int)

(defcfun ("derwin" derwin) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfun ("doupdate" doupdate) :int)

(defcfun ("dupwin" dupwin) :pointer
  (arg0 :pointer))

(defcfun ("echo" echo) :int)

(defcfun ("echochar" echochar) :int
  (arg0 :int))

(defcfun ("erase" erase) :int)

(defcfun ("endwin" endwin) :int)

(defcfun ("erasechar" erasechar) :char)

(defcfun ("filter" filter) :void)

(defcfun ("flash" flash) :int)

(defcfun ("flushinp" flushinp) :int)

(defcfun ("getbkgd" getbkgd) :int
  (arg0 :pointer))

(defcfun ("getch" getch) :int)

(defcfun ("getnstr" getnstr) :int
  (arg0 :string)
  (arg1 :int))

(defcfun ("getstr" getstr) :int
  (arg0 :string))

(defcfun ("getwin" getwin) :pointer
  (arg0 :pointer))

(defcfun ("halfdelay" halfdelay) :int
  (arg0 :int))

(defcfun ("has_colors" has_colors) :int)

(defcfun ("has_ic" has_ic) :int)

(defcfun ("has_il" has_il) :int)

(defcfun ("hline" hline) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("idcok" idcok) :void
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("idlok" idlok) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("immedok" immedok) :void
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("inch" inch) :int)

(defcfun ("inchnstr" inchnstr) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("inchstr" inchstr) :int
  (arg0 :pointer))

(defcfun ("initscr" initscr) :pointer)

(defcfun ("init_color" init_color) :int
  (arg0 :short)
  (arg1 :short)
  (arg2 :short)
  (arg3 :short))

(defcfun ("init_pair" init_pair) :int
  (arg0 :short)
  (arg1 :short)
  (arg2 :short))

(defcfun ("innstr" innstr) :int
  (arg0 :string)
  (arg1 :int))

(defcfun ("insch" insch) :int
  (arg0 :int))

(defcfun ("insdelln" insdelln) :int
  (arg0 :int))

(defcfun ("insertln" insertln) :int)

(defcfun ("insnstr" insnstr) :int
  (arg0 :string)
  (arg1 :int))

(defcfun ("insstr" insstr) :int
  (arg0 :string))

(defcfun ("instr" instr) :int
  (arg0 :string))

(defcfun ("intrflush" intrflush) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("isendwin" isendwin) :int)

(defcfun ("is_linetouched" is_linetouched) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("is_wintouched" is_wintouched) :int
  (arg0 :pointer))

(defcfun ("keyname" keyname) :string
  (arg0 :int))

(defcfun ("keypad" keypad) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("killchar" killchar) :char)

(defcfun ("leaveok" leaveok) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("longname" longname) :string)

(defcfun ("meta" meta) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("move" move) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("mvaddch" mvaddch) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvaddchnstr" mvaddchnstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))

(defcfun ("mvaddchstr" mvaddchstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :pointer))

(defcfun ("mvaddnstr" mvaddnstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string)
  (arg3 :int))

(defcfun ("mvaddstr" mvaddstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string))

(defcfun ("mvchgat" mvchgat) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :short)
  (arg5 :pointer))

(defcfun ("mvcur" mvcur) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("mvdelch" mvdelch) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("mvderwin" mvderwin) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvgetch" mvgetch) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("mvgetnstr" mvgetnstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string)
  (arg3 :int))

(defcfun ("mvgetstr" mvgetstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string))

(defcfun ("mvhline" mvhline) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("mvinch" mvinch) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("mvinchnstr" mvinchnstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))

(defcfun ("mvinchstr" mvinchstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :pointer))

(defcfun ("mvinnstr" mvinnstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string)
  (arg3 :int))

(defcfun ("mvinsch" mvinsch) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvinsnstr" mvinsnstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string)
  (arg3 :int))

(defcfun ("mvinsstr" mvinsstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string))

(defcfun ("mvinstr" mvinstr) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :string))

(defcfun ("mvvline" mvvline) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("mvwaddch" mvwaddch) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("mvwaddchnstr" mvwaddchnstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :int))

(defcfun ("mvwaddchstr" mvwaddchstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfun ("mvwaddnstr" mvwaddnstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfun ("mvwaddstr" mvwaddstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string))

(defcfun ("mvwchgat" mvwchgat) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :short)
  (arg6 :pointer))

(defcfun ("mvwdelch" mvwdelch) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvwgetch" mvwgetch) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvwgetnstr" mvwgetnstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfun ("mvwgetstr" mvwgetstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string))

(defcfun ("mvwhline" mvwhline) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfun ("mvwin" mvwin) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvwinch" mvwinch) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("mvwinchnstr" mvwinchnstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :int))

(defcfun ("mvwinchstr" mvwinchstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfun ("mvwinnstr" mvwinnstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfun ("mvwinsch" mvwinsch) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("mvwinsnstr" mvwinsnstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfun ("mvwinsstr" mvwinsstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string))

(defcfun ("mvwinstr" mvwinstr) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string))

(defcfun ("mvwvline" mvwvline) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfun ("napms" napms) :int
  (arg0 :int))

(defcfun ("newpad" newpad) :pointer
  (arg0 :int)
  (arg1 :int))

(defcfun ("newterm" newterm) :pointer
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfun ("newwin" newwin) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("nl" nl) :int)

(defcfun ("nocbreak" nocbreak) :int)

(defcfun ("nodelay" nodelay) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("noecho" noecho) :int)

(defcfun ("nonl" nonl) :int)

(defcfun ("noqiflush" noqiflush) :void)

(defcfun ("noraw" noraw) :int)

(defcfun ("notimeout" notimeout) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("overlay" overlay) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("overwrite" overwrite) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("pair_content" pair_content) :int
  (arg0 :short)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfun ("PAIR_NUMBER" PAIR_NUMBER) :int
  (arg0 :int))

(defcfun ("pechochar" pechochar) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("pnoutrefresh" pnoutrefresh) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int))

(defcfun ("prefresh" prefresh) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int))

(defcfun ("putp" putp) :int
  (arg0 :string))

(defcfun ("putwin" putwin) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("qiflush" qiflush) :void)

(defcfun ("raw" raw) :int)

(defcfun ("redrawwin" redrawwin) :int
  (arg0 :pointer))

(defcfun ("refresh" refresh) :int)

(defcfun ("resetty" resetty) :int)

(defcfun ("reset_prog_mode" reset_prog_mode) :int)

(defcfun ("reset_shell_mode" reset_shell_mode) :int)

(defcfun ("ripoffline" ripoffline) :int
  (arg0 :int)
  (arg1 :pointer))

(defcfun ("savetty" savetty) :int)

(defcfun ("scr_dump" scr_dump) :int
  (arg0 :string))

(defcfun ("scr_init" scr_init) :int
  (arg0 :string))

(defcfun ("scrl" scrl) :int
  (arg0 :int))

(defcfun ("scroll" scroll) :int
  (arg0 :pointer))

(defcfun ("scrollok" scrollok) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("scr_restore" scr_restore) :int
  (arg0 :string))

(defcfun ("scr_set" scr_set) :int
  (arg0 :string))

(defcfun ("setscrreg" setscrreg) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("set_term" set_term) :pointer
  (arg0 :pointer))

(defcfun ("slk_attroff" slk_attroff) :int
  (arg0 :int))

(defcfun ("slk_attr_off" slk_attr_off) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("slk_attron" slk_attron) :int
  (arg0 :int))

(defcfun ("slk_attr_on" slk_attr_on) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("slk_attrset" slk_attrset) :int
  (arg0 :int))

(defcfun ("slk_attr" slk_attr) :pointer)

(defcfun ("slk_attr_set" slk_attr_set) :int
  (arg0 :pointer)
  (arg1 :short)
  (arg2 :pointer))

(defcfun ("slk_clear" slk_clear) :int)

(defcfun ("slk_color" slk_color) :int
  (arg0 :short))

(defcfun ("slk_init" slk_init) :int
  (arg0 :int))

(defcfun ("slk_label" slk_label) :string
  (arg0 :int))

(defcfun ("slk_noutrefresh" slk_noutrefresh) :int)

(defcfun ("slk_refresh" slk_refresh) :int)

(defcfun ("slk_restore" slk_restore) :int)

(defcfun ("slk_set" slk_set) :int
  (arg0 :int)
  (arg1 :string)
  (arg2 :int))

(defcfun ("slk_touch" slk_touch) :int)

(defcfun ("standout" standout) :int)

(defcfun ("standend" standend) :int)

(defcfun ("start_color" start_color) :int)

(defcfun ("subpad" subpad) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfun ("subwin" subwin) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfun ("syncok" syncok) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("termattrs" termattrs) :int)

(defcfun ("termname" termname) :string)

(defcfun ("tigetflag" tigetflag) :int
  (arg0 :string))

(defcfun ("tigetnum" tigetnum) :int
  (arg0 :string))

(defcfun ("tigetstr" tigetstr) :string
  (arg0 :string))

(defcfun ("timeout" timeout) :void
  (arg0 :int))

(defcfun ("touchline" touchline) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("touchwin" touchwin) :int
  (arg0 :pointer))

(defcfun ("typeahead" typeahead) :int
  (arg0 :int))

(defcfun ("ungetch" ungetch) :int
  (arg0 :int))

(defcfun ("untouchwin" untouchwin) :int
  (arg0 :pointer))

(defcfun ("use_env" use_env) :void
  (arg0 :int))

(defcfun ("vidattr" vidattr) :int
  (arg0 :int))

(defcfun ("vidputs" vidputs) :int
  (arg0 :int)
  (arg1 :pointer))

(defcfun ("vline" vline) :int
  (arg0 :int)
  (arg1 :int))

(defcfun ("vwprintw" vwprintw) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfun ("vw_printw" vw_printw) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfun ("vwscanw" vwscanw) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfun ("vw_scanw" vw_scanw) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfun ("waddch" waddch) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("waddchnstr" waddchnstr) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfun ("waddchstr" waddchstr) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("waddnstr" waddnstr) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int))

(defcfun ("waddstr" waddstr) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfun ("wattron" wattron) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wattroff" wattroff) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wattrset" wattrset) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wattr_get" wattr_get) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfun ("wattr_on" wattr_on) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfun ("wattr_off" wattr_off) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfun ("wattr_set" wattr_set) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :short)
  (arg3 :pointer))

(defcfun ("wbkgd" wbkgd) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wbkgdset" wbkgdset) :void
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wborder" wborder) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfun ("wchgat" wchgat) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :short)
  (arg4 :pointer))

(defcfun ("wclear" wclear) :int
  (arg0 :pointer))

(defcfun ("wclrtobot" wclrtobot) :int
  (arg0 :pointer))

(defcfun ("wclrtoeol" wclrtoeol) :int
  (arg0 :pointer))

(defcfun ("wcolor_set" wcolor_set) :int
  (arg0 :pointer)
  (arg1 :short)
  (arg2 :pointer))

(defcfun ("wcursyncup" wcursyncup) :void
  (arg0 :pointer))

(defcfun ("wdelch" wdelch) :int
  (arg0 :pointer))

(defcfun ("wdeleteln" wdeleteln) :int
  (arg0 :pointer))

(defcfun ("wechochar" wechochar) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("werase" werase) :int
  (arg0 :pointer))

(defcfun ("wgetch" wgetch) :int
  (arg0 :pointer))

(defcfun ("wgetnstr" wgetnstr) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int))

(defcfun ("wgetstr" wgetstr) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfun ("whline" whline) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("winch" winch) :int
  (arg0 :pointer))

(defcfun ("winchnstr" winchnstr) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfun ("winchstr" winchstr) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfun ("winnstr" winnstr) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int))

(defcfun ("winsch" winsch) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("winsdelln" winsdelln) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("winsertln" winsertln) :int
  (arg0 :pointer))

(defcfun ("winsnstr" winsnstr) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int))

(defcfun ("winsstr" winsstr) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfun ("winstr" winstr) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfun ("wmove" wmove) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("wnoutrefresh" wnoutrefresh) :int
  (arg0 :pointer))

(defcfun ("wredrawln" wredrawln) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("wrefresh" wrefresh) :int
  (arg0 :pointer))

(defcfun ("wscrl" wscrl) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wsetscrreg" wsetscrreg) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("wstandout" wstandout) :int
  (arg0 :pointer))

(defcfun ("wstandend" wstandend) :int
  (arg0 :pointer))

(defcfun ("wsyncdown" wsyncdown) :void
  (arg0 :pointer))

(defcfun ("wsyncup" wsyncup) :void
  (arg0 :pointer))

(defcfun ("wtimeout" wtimeout) :void
  (arg0 :pointer)
  (arg1 :int))

(defcfun ("wtouchln" wtouchln) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfun ("wvline" wvline) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfun ("assume_default_colors" assume_default_colors) :int
  (arg0 :int)
  (arg1 :int))
