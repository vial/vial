(defpackage :ncurses
  (:use :common-lisp :cffi)
  (:export 
   #:addstr
   #:cleanup
   #:clear
   #:clrtoeol
   #:COLS
   #:getch
   #:init
   #:init_pair
   #:LINES
   #:move
   #:newwin
   #:refresh
   #:waddstr
   #:wmove
   #:wrefresh
   #:wclear

   #:A_NORMAL
   #:A_ATTRIBUTES
   #:A_CHARTEXT	
   #:A_COLOR
   #:A_STANDOUT
   #:A_UNDERLINE
   #:A_REVERSE
   #:A_BLINK
   #:A_DIM 
   #:A_BOLD
   #:COLOR_BLACK
   #:COLOR_RED
   #:COLOR_GREEN
   #:COLOR_YELLOW
   #:COLOR_BLUE
   #:COLOR_MAGENTA
   #:COLOR_CYAN
   #:COLOR_WHITE))

(in-package :ncurses)
; load the SWIG generated file
;(load "./ncurses-cffi/ncurses-cffi")

(defconstant NCURSES_ATTR_SHIFT       8)
(defmacro NCURSES_BITS (mask shift) `(ash ,mask (+ ,shift NCURSES_ATTR_SHIFT)))

(defconstant A_NORMAL	0)
;(defconstant A_ATTRIBUTES	NCURSES_BITS(~(1UL - 1UL),0))
;(defconstant A_CHARTEXT	(NCURSES_BITS(1UL,0) - 1UL))
;(defconstant A_COLOR		NCURSES_BITS(((1UL) << 8) - 1UL,0))
(defconstant A_STANDOUT	 (NCURSES_BITS 1 8))
(defconstant A_UNDERLINE (NCURSES_BITS 1 9))
(defconstant A_REVERSE	 (NCURSES_BITS 1 10))
(defconstant A_BLINK	 (NCURSES_BITS 1 11))
(defconstant A_DIM     	 (NCURSES_BITS 1 12))
(defconstant A_BOLD 	 (NCURSES_BITS 1 13))
#|
#define A_ALTCHARSET	NCURSES_BITS(1UL,14)
#define A_INVIS		NCURSES_BITS(1UL,15)
#define A_PROTECT	NCURSES_BITS(1UL,16)
#define A_HORIZONTAL	NCURSES_BITS(1UL,17)
#define A_LEFT		NCURSES_BITS(1UL,18)
#define A_LOW		NCURSES_BITS(1UL,19)
#define A_RIGHT		NCURSES_BITS(1UL,20)
#define A_TOP		NCURSES_BITS(1UL,21)
#define A_VERTICAL	NCURSES_BITS(1UL,22)

#define COLOR_PAIR(n)	NCURSES_BITS(n, 0)
#define PAIR_NUMBER(a)	((int)(((a) & A_COLOR) >> NCURSES_ATTR_SHIFT))
|#

(define-foreign-library libncurses
 (t (:default "libncurses")))

(use-foreign-library libncurses)

(defcvar ("stdscr" *stdscr*) :pointer)
(defcvar ("curscr" *curscr*) :pointer)
(defcvar ("newscr" *newscr*) :pointer)
(defcvar ("LINES" LINES) :int)
(defcvar ("COLS" COLS) :int)
(defcvar ("TABSIZE" TABSIZE) :int)

(defun move (y x) (wmove *stdscr* y x))
(defun addstr (str) (waddstr *stdscr* str))
(defun refresh () (wrefresh *stdscr*))
(defun clear () (wclear *stdscr*))

(defun init ()
 (if (null-pointer-p *stdscr*)
  (initscr)
  (refresh))
 (keypad *stdscr* 1)
 (raw)
 (noecho)
 (move 0 0))

(defun cleanup ()
 (endwin))



