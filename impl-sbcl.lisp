;;; This software is free under the zlib license, see LICENSE for details
(in-package :vial)

(defparameter *stdin-handler* nil)

(defun init-lisp-implementation ()
 (setf *stdin-handler* (sb-sys:add-fd-handler 0 :input
                        (lambda (_) (get-key-from-keyboard)))))

(defun cleanup-lisp-implementation ()
 (when *stdin-handler*
  (sb-sys:remove-fd-handler *stdin-handler*)))

(defun wait-for-event ()
 ;(get-key-from-keyboard)
 (sb-sys:serve-event)
)
